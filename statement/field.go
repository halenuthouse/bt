package statement

import "regexp"

type Column struct {
	Family    string
	Qualifier string
}

// FieldInfo represents the meta data for a field
type FieldInfo struct {
	Name  string
	Width int
}

// Field represents a column
type Field struct {
	Column
	All       bool
	Options   *ColumnOption
	BTFields  []FieldInfo
}

func ParseColumn(value string) Field {
	re := regexp.MustCompile(`(?m)(\w*):(\w*)(.*)`)
	for _, match := range re.FindAllStringSubmatch(value, -1) {
		return Field {
			Column: Column{
				Family: match[1],
				Qualifier: match[2],
			},
			Options: ParseColumnOption(match[3]),
		}
	}

	return Field{}
}

func (column *Column) Name() string {
	return column.Family + ":" + column.Qualifier
}
