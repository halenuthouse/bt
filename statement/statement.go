package statement

import (
	"fmt"
	"regexp"
)

// Statement represents a Statement from the bigtable database
type Statement struct {
	Raw         string
	Method      string
	SubFunction string
	Fields      []Field
	TableName   string
	Filters     []QueryFilter
	Sets        []SetField
	Limit       int64
}

// SetField is the struct to define column value changes
type SetField struct {
	Column
	Value     []byte
}

// QueryFilter is the struct for the filters
type QueryFilter struct {
	Field    string
	Opr      string
	Values   []string
	IsKeyArg bool
}

// FlagKeyArgs returns
func (stmt *Statement) FlagKeyArgs(pattern string) {
	re := regexp.MustCompile(`(?m){{\.(\w*)}}`)
	for _, match := range re.FindAllStringSubmatch(pattern, -1) {
		for i := range stmt.Filters {
			if stmt.Filters[i].Field == match[1] {
				stmt.Filters[i].IsKeyArg = true
			}
		}
	}
}

// MapWhereByOpr returns
func (stmt *Statement) MapWhereByOpr(opr string) map[string]interface{} {
	ret := map[string]interface{}{}
	for _, where := range stmt.Filters {
		if where.Opr == opr {
			ret[where.Field] = where.Values[0]
		}
	}

	return ret
}

// FindFilterField returns the QueryFilter for the specified field
func (stmt *Statement) FindFilterField(field string) *QueryFilter {
	for _, where := range stmt.Filters {
		if where.Field == field {
			return &where
		}
	}

	return nil
}

// MapWhereToArgs maps the where clauses to an array of strings containing arguments
func (stmt *Statement) MapWhereToArgs(args []string) (map[string]*QueryFilter, bool) {
	allMatch := true
	matches := map[string]*QueryFilter{}

	for _, arg := range args {
		if match := stmt.FindFilterField(arg); match != nil {
			matches[arg] = match
		} else {
			allMatch = false
		}
	}

	return matches, allMatch
}

func hasEqualsAndInKey(key string, whereEquals map[string]interface{}, whereIn map[string]interface{}) error {
	if _, ok := whereEquals[key]; ok {
		if _, ok := whereIn[key]; ok {
			return fmt.Errorf("The key %s cannot have both IN clause and =", key)
		}
	}

	return nil
}
