package statement

import (
	"strings"
)

// Split will break up the statement
func Split(s string) []string {
	return strings.Split(s, " ")
}

// Join will put the statement back together
func Join(stmt []string, flagsToRemove ...string) string {
	plain := ""
	for _, value := range stmt {
		if value[0] == '-' {
			split := strings.Split(value, "=")
			skip := false
			for _, arg := range flagsToRemove {
				if split[0] == "-" + arg {
					skip = true
				}
			}
			if skip {
				continue
			}
		}
		plain += value + " "
	}

	return strings.TrimSpace(plain)
}
