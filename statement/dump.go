package statement

import "fmt"

// Dump is used for debugging purposes. It returns a string contain the dumped statement.
func (stmt *Statement) Dump() string {
	ret := fmt.Sprintf("Method:    %s\n", stmt.Method)

	if stmt.SubFunction != "" {
		ret += fmt.Sprintf("SubFunction:   %s\n", stmt.SubFunction)
	}

	ret += dumpFields(stmt.Fields)

	ret += fmt.Sprintf("TableName:    %s\n", stmt.TableName)

	ret += dumpFilter(stmt.Filters)
	ret += dumpSets(stmt.Sets)

	if stmt.Limit != 0 {
		ret += fmt.Sprintf("Limit:    %d\n", stmt.Limit)
	}

	return ret
}

func dumpSets(sets []SetField) string {
	ret := ""
	for _, set := range sets {
		ret += fmt.Sprintf("\t%s = %s\n", set.Column.Name(), set.Value)
	}
	return ret
}

func dumpFilter(filters []QueryFilter) string {
	ret := fmt.Sprintf("Filters:\n")

	for _, filter := range filters {
		ret += fmt.Sprintf("\t%s %s\n", filter.Field, filter.Opr)
		for _, value := range filter.Values {
			ret += fmt.Sprintf("\t\t%s\n", value)
		}
	}
	return ret
}

func dumpFields(fields []Field) string {
	ret := fmt.Sprintf("Fields:\n")
	for _, field := range fields {
		if field.All {
			ret += "\tAll\n"
		} else {
			ret += "\t" + field.Column.Name() + "\n"
		}

		if field.Options != nil {
			if field.Options.ResizeStrategy != "" {
				ret += fmt.Sprintf("\t\tResize: %s\n", field.Options.ResizeStrategy)
			}
			if field.Options.Format != "" {
				ret += fmt.Sprintf("\t\tFormat: %s\n", field.Options.Format)
			}
			if field.Options.Size != 0 {
				ret += fmt.Sprintf("\t\tSize: %d\n", field.Options.Size)
			}
		}

		//if len(field.BTFields) != 0 {
		//	ret += "\t\tBTFields\n"
		//	for _, bt := range field.BTFields {
		//		fmt.Sprintf()
		//	}
		//}
		//BTFields  []FieldInfo
	}

	return ret
}