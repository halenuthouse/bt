package statement

import (
	"strconv"
	"strings"
)

const (
	// Width options
	Auto = ""		// Can be resized (default)
	Full = "full"	// Cannot be resized

	// Format options
	Native = ""  	// Display column as is (default)
	JSON = "JSON"	// Treat as a JSON stream
	HEX = "HEX"     // Display as hex dump
	Hide = "hide"   // Don't display column
)

// Options represents the options applied to a column
type ColumnOption struct {
	Size  	       int
	ResizeStrategy string
	Format 		   string
}

func ParseColumnOption(str string) (ret *ColumnOption) {
	split := strings.Split(str, "|")
	ret = &ColumnOption{}
	for _, part := range split {
		r := strings.NewReplacer("[","","]","","(","",")","")
		part = r.Replace(part)
		switch strings.ToLower(part) {
		case "json":
			ret.Format = JSON
		case "hex":
			ret.Format = HEX
		case "hide":
			ret.Format = Hide
		case "full":
			ret.ResizeStrategy = Full
		default:
			if width, err := strconv.Atoi(part); err == nil {
				ret.Size = width
			}
		}
	}

	return ret
}

func (option *ColumnOption) IsEmpty() bool {
	if option == nil {
		return true
	}

	return option.Format == "" && option.Size == 0 && option.ResizeStrategy == ""
}