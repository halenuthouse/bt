package server

import (
	"github.com/labstack/echo"
	"net/http"
	"time"
)

func (h handler) shutdown(c echo.Context) error {
	go func() {
		time.Sleep(3000)
		h.e.Shutdown(c.Request().Context())
	}()

	return c.NoContent(http.StatusOK)
}
