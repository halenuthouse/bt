package server

import (
	"bt/execute"
	"bt/help"
	"bt/statement"
	"encoding/json"
	"github.com/labstack/echo"
	"io/ioutil"
	"net/http"
	"os"
)

type (
	Results struct {
		Errors    []string `json:"errors"`
		Help      string   `json:"help"`
		Statement string   `json:"statement"`
		Response  string   `json:"response"`
		Records   interface{}   `json:"records"`
	}
)

func (h handler) executeStatement(c echo.Context) error {
	raw := c.QueryParam("stmt")
	results := Results{Statement: raw}

	temp, err := ioutil.TempFile("", "bt-*.json")
	if err != nil {
		results.Errors = []string{err.Error()}
	} else {
		defer os.Remove(temp.Name())

		stmt := statement.Split(raw)
		stmt = append(stmt, "-fmt=json")
		response, hlp, _, err := execute.Process(temp, stmt)
		if err != nil {
			results.Errors = []string{err.Error()}
		}

		if err = temp.Close(); err != nil {
			results.Errors = append(results.Errors, err.Error())
		}

		if hlp != "" {
			results.Help =  help.Display(hlp)
		}

		results.Response = response
		records, err := ioutil.ReadFile(temp.Name())
		if err != nil {
			results.Errors = append(results.Errors, err.Error())
		} else {
			if len(records) != 0 {
				results.Records = json.RawMessage(records)
			}
		}
	}
	return c.JSON(http.StatusOK, results)
}

