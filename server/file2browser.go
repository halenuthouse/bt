package server

import (
	"github.com/labstack/echo"
	"os"
)

func (h handler) file2Browser(c echo.Context) error {
	defer h.shutdown(c)
	defer os.Remove(h.html2browserFile)

	return c.File(h.html2browserFile)
}
