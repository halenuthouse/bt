package server

import (
	"bt/browser"
	"bt/statement"
	"fmt"
	"github.com/labstack/echo"
	"net"
	"net/url"
	"strconv"
)

const (
	ExecStatementPath = "/api/execute"
	ExecShutdown      = "/api/shutdown"
	ExecHTML2Browser  = "/api/file2Browser"
)

type handler struct {
	e 	 *echo.Echo
	port int

	html2browserFile string
}

// LaunchBrowserFile will launch the server, and a browser that will request the file.
func LaunchBrowserFile(fileName string, port int) error {
	h, err := initialize(port)
	if err != nil {
		return err
	}

	h.html2browserFile = fileName
	h.e.GET(ExecHTML2Browser, h.file2Browser)

	return h.startServer()
}

// Launch will launch the server with the specified port. If the port is 0, it will find a port to use.
func Launch(port int) error {
	h, err := initialize(port)
	if err != nil {
		return err
	}

	h.e.GET(ExecStatementPath, h.executeStatement)
	h.e.GET(ExecShutdown, h.shutdown)

	return h.startServer()
}

func ExecBrowserURL() (string, int, error) {
	url, port, err := address(0)
	if err != nil {
		return "", 0, err
	}

	return fmt.Sprintf("%s%s", url, ExecHTML2Browser), port, nil
}

func ExecStatementURL(port int, stmt []string) (string, int, error) {
	encodedStmt := ""
	if len(stmt) > 0 {
		encodedStmt = browser.EncodeStatement(stmt)
	}

	url, port, err := address(port)
	if err != nil {
		return "", 0, err
	}

	return fmt.Sprintf("%s%s?stmt=%s", url, ExecStatementPath, encodedStmt), port, nil
}

func address(port int) (string, int, error) {
	port, err := findPort(port)
	if err != nil {
		return "", 0, err
	}
	return fmt.Sprintf("http://localhost:%d", port), port, nil
}

// findPort will search for an open port if a port isn't specified specified
func findPort(port int) (int, error) {
	const (
		fromPort = 4000
		toPort   = 4100
	)
	if port == 0 {
		for p := fromPort; p <= toPort; p++ {
			l, err := net.Listen("tcp", ":"+strconv.Itoa(p))
			defer l.Close()

			if err == nil {
				return p, nil
			}
		}

		return 0, fmt.Errorf("unable to find an open port between %d-%d", fromPort, toPort)
	}
	return port, nil
}

func (h *handler) startServer() error {
	sPort := ":" + strconv.Itoa(h.port)
	h.e.Logger.Infof("Started BT Server on port %s", sPort)

	h.e.HideBanner = true
	return h.e.Start(sPort)
}

func initialize(port int) (*handler, error) {
	// if port not specified...then find us a port
	port, err := findPort(port)
	if err != nil {
		return nil, err
	}

	return &handler { e: echo.New(), port: port }, nil
}

func encodeStatement(stmt []string) string {
	plain := statement.Join(stmt, "s", "server", "fmt")
	return url.PathEscape(plain)
}