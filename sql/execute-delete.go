package sql

import (
	"context"
	"fmt"

	"sort"
	"strings"

	"cloud.google.com/go/bigtable"
	"bt/config"
)

func (stmt *Statement) executeDelete(cfg config.Config, options Options) error {
	switch stmt.SubFunction {
	case "LIST":
		ctx := context.Background()
		admin, err := openAdmin(ctx, options, cfg)
		defer admin.Close()

		if err != nil {
			return fmt.Errorf("connecting: %v", err)
		}

		tables, err := admin.Tables(ctx)
		if err != nil {
			return fmt.Errorf("unable to query the list of tables: %v", err)
		}

		sort.Strings(tables)
		fmt.Println("Table Name")
		fmt.Println("-----------")
		for _, tableName := range tables {
			fmt.Println(tableName)
		}
	case "COUNT":
		ctx := context.Background()
		client, err := openClient(ctx, options, cfg)
		defer client.Close()

		if err != nil {
			return fmt.Errorf("Connecting: %v", err)
		}

		btTable := client.Open(stmt.TableName)
		count := 0

		err = btTable.ReadRows(
			ctx,
			bigtable.InfiniteRange(""),
			func(_ bigtable.Row) bool {
				count++
				return true
			},
			bigtable.RowFilter(bigtable.StripValueFilter()),
		)
		if err != nil {
			return fmt.Errorf("Reading rows: %v", err)
		}
		fmt.Println("Record Count for", stmt.TableName, "is", count)

	case "SCHEMA":
		ctx := context.Background()
		admin, err := openAdmin(ctx, options, cfg)
		defer admin.Close()

		if err != nil {
			return fmt.Errorf("Connecting: %v", err)
		}

		ti, err := admin.TableInfo(ctx, stmt.TableName)
		if err != nil {
			return fmt.Errorf("Getting table info: %v", err)
		}

		PrintLeftJustified("Family Name", 30)
		fmt.Println("GC Policy")
		fmt.Print(strings.Repeat("-", 29))
		fmt.Print(" ")
		fmt.Println(strings.Repeat("-", 20))
		for _, fam := range ti.FamilyInfos {
			PrintLeftJustified(fam.Name, 30)
			fmt.Println(fam.GCPolicy)
		}
	}

	return nil
}
