package parse

import (
	"fmt"
	"io"
	"strings"

	"bt/statement"
)

// Parser represents a parser.
type Parser struct {
	s   *Scanner
	buf struct {
		tok Token  // last read token
		lit string // last read literal
		n   int    // buffer size (max=1)
	}
}

// NewParser returns a new instance of Parser.
func NewParser(r io.Reader) *Parser {
	return &Parser{s: NewScanner(r)}
}

// Parse parses a SQL SELECT statement.
func (p *Parser) Parse() (statement.Statement, statement.Options, error) {
	stmt := statement.Statement{}
	options := statement.Options{}
	var err error

	tok, lit := p.scanIgnoreWhitespace(&options)
	stmt.Method = strings.ToUpper(lit)

	switch tok {
	case SELECT:
		err = p.parseSelect(&stmt, &options)
	case INSERT:
		err = fmt.Errorf("%q not supported yet", stmt.Method)
	case UPDATE:
		err = fmt.Errorf("%q not supported yet", stmt.Method)
		//	case DELETE:
		//		err = p.parseDelete(&stmt, &options)
	case DROP:
		err = fmt.Errorf("%q not supported yet", stmt.Method)
	case ALTER:
		err = fmt.Errorf("%q not supported yet", stmt.Method)
	case TABLE:
		err = p.parseTable(&stmt, &options)
	default:
		err = fmt.Errorf("found %q, expected SELECT, INSERT, UPDATE, DELETE, DROP, ALTER, or TABLE", lit)
	}

	// Return the successfully parsed statement.
	return stmt, options, err
}

// scan returns the next token from the underlying scanner.
// If a token has been unscanned then read that instead.
func (p *Parser) scan() (tok Token, lit string) {
	// If we have a token on the buffer, then return it.
	if p.buf.n != 0 {
		p.buf.n = 0
		return p.buf.tok, p.buf.lit
	}

	// Otherwise read the next token from the scanner.
	tok, lit = p.s.Scan()

	// Save it to the buffer in case we unscan later.
	p.buf.tok, p.buf.lit = tok, lit

	return
}

// scanIgnoreWhitespace scans the next non-whitespace token.
func (p *Parser) scanIgnoreWhitespace(options *statement.Options) (tok Token, lit string) {
	tok, lit = p.scan()
	if tok == WS {
		tok, lit = p.scan()
	}

	if lit != "" && lit[:1] == "-" {
		option := ""
		for {
			if tok == EOF {
				break
			}
			if tok == WS {
				tok, lit = p.scan()
				break
			}
			option += lit
			tok, lit = p.scan()
		}
		*options = append(*options, option)
	}
	return
}

// unscan pushes the previously read token back onto the buffer.
func (p *Parser) unscan() { p.buf.n = 1 }
