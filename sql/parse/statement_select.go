package parse

import (
	"fmt"

	"bt/statement"
)

func (p *Parser) parseSelect(stmt *statement.Statement, options *statement.Options) error {

	// Next we should loop over all our comma-delimited fields.
	for {
		// Read a field.
		tok, lit := p.scanIgnoreWhitespace(options)
		if tok == FROM {
			break
		}

		if tok != IDENT && tok != ASTERISK {
			return fmt.Errorf("found %q, expected field, * or FROM", lit)
		}

		stmt.Fields = append(stmt.Fields, lit)

		// If the next token is not a comma then break the loop.
		tok, _ = p.scanIgnoreWhitespace(options)
		if tok == FROM {
			break
		}

		if tok != COMMA {
			p.unscan()
			break
		}
	}

	if len(stmt.Fields) == 0 {
		stmt.Fields = append(stmt.Fields, "*")
	}

	// Then we should read the table name.
	tok, lit := p.scanIgnoreWhitespace(options)
	if tok != IDENT {
		return fmt.Errorf("found %q, expected table name", lit)
	}
	stmt.TableName = lit

	// Optionally, we should read the WHERE clause
	tok, lit = p.scanIgnoreWhitespace(options)
	if tok == WHERE {
		// Next we should loop over all our comma-delimited fields.
		for {
			where := statement.QueryFilter{}
			// Read a field.
			tok, lit = p.scanIgnoreWhitespace(options)
			if tok != IDENT {
				return fmt.Errorf("found %q, expected field", lit)
			}
			where.Field = lit

			tok, lit = p.scanIgnoreWhitespace(options)
			if tok != EQUALS && tok != CONTAINS {
				return fmt.Errorf("found %q, expected operator", lit)
			}
			where.Opr = lit

			tok, lit = p.scanIgnoreWhitespace(options)
			where.Value = lit

			stmt.Filter = append(stmt.Filter, where)

			// If the next token is not an AND then break the loop.
			tok, lit = p.scanIgnoreWhitespace(options)
			if tok != AND {
				p.unscan()
				break
			}
		}
	}

	if tok != EOF {
		return fmt.Errorf("found unexpected %q", lit)
	}

	return nil
}
