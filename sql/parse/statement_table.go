package parse

import (
	"fmt"

	"bt/statement"
)

func (p *Parser) parseTable(stmt *statement.Statement, options *statement.Options) error {

	tok, lit := p.scanIgnoreWhitespace(options)
	switch tok {
	case LIST:
		tok, lit := p.scanIgnoreWhitespace(options)
		if tok != EOF {
			return fmt.Errorf("found unexpected %q", lit)
		}
		stmt.SubFunction = "LIST"
	case COUNT:
		tok, lit := p.scanIgnoreWhitespace(options)
		if tok != IDENT {
			return fmt.Errorf("found %q, expected table name", lit)
		}
		stmt.TableName = lit
		stmt.SubFunction = "COUNT"
	case SCHEMA:
		tok, lit := p.scanIgnoreWhitespace(options)
		if tok != IDENT {
			return fmt.Errorf("found %q, expected table name", lit)
		}
		stmt.TableName = lit
		stmt.SubFunction = "SCHEMA"
	default:
		return fmt.Errorf("found %q, expected LIST or SCHEMA", lit)
	}

	return nil
}
