package parse

// Token represents a lexical token.
type Token int

const (
	// Special tokens
	ILLEGAL Token = iota
	EOF
	WS

	// Literals
	IDENT // main

	// Misc characters
	ASTERISK // *
	COMMA    // ,
	EQUALS   // =

	// Keywords
	SELECT
	FROM
	WHERE
	AND
	CONTAINS
	INSERT
	UPDATE
	DELETE
	DROP
	ALTER
	TABLE
	FAMILY
	LIST
	SCHEMA
	COUNT
)
