package parse

import (
	"fmt"

	"bt/sql"
)

func (p *Parser) parseDelete(stmt *sql.Statement, options *sql.Options) error {

	stmt.Fields = append(stmt.Fields, "*")

	tok, lit := p.scanIgnoreWhitespace(options)
	if tok != FROM {
		return fmt.Errorf("found %q, expected field, FROM", lit)
	}

	// Then we should read the table name.
	tok, lit = p.scanIgnoreWhitespace(options)
	if tok != IDENT {
		return fmt.Errorf("found %q, expected table name", lit)
	}
	stmt.TableName = lit

	tok, lit = p.scanIgnoreWhitespace(options)
	if tok != WHERE {
		return fmt.Errorf("found %d, expected field, WHERE", lit)
	}

	for {
		where := sql.WhereClause{}
		// Read a field.
		tok, lit = p.scanIgnoreWhitespace(options)
		if tok != IDENT {
			return fmt.Errorf("found %q, expected field", lit)
		}
		where.Field = lit

		tok, lit = p.scanIgnoreWhitespace(options)
		if tok != EQUALS && tok != CONTAINS {
			return fmt.Errorf("found %q, expected operator", lit)
		}
		where.Opr = lit

		tok, lit = p.scanIgnoreWhitespace(options)
		where.Value = lit

		stmt.Where = append(stmt.Where, where)

		// If the next token is not an AND then break the loop.
		tok, lit = p.scanIgnoreWhitespace(options)
		if tok != AND {
			p.unscan()
			break
		}
	}

	if tok != EOF {
		return fmt.Errorf("found unexpected %q", lit)
	}

	return nil
}
