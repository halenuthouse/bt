package browser

import (
	"log"
	"net/url"
	"os/exec"
	"runtime"

	"bt/statement"
)

// Open the default browser for the OS and direct to the specified URL
func Open(url string) *exec.Cmd {
	var cmd *exec.Cmd

	switch runtime.GOOS {
	case "linux":
		cmd = exec.Command("xdg-open", url)
	case "windows":
		cmd = exec.Command("rundll32", "url.dll,FileProtocolHandler", url)
	case "darwin":
		cmd = exec.Command("open", url)
	default:
		log.Fatal("unsupported platform")
	}

	if err := cmd.Start(); err != nil {
		log.Fatal(err)
	}

	return cmd
}

// EncodeStatement encodes the statement
func EncodeStatement(stmt []string) string {
	plain := statement.Join(stmt, "s", "server", "fmt")
	return url.PathEscape(plain)
}
