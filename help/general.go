package help

import (
	"bt/config"
)

func displayGeneralHelp() string {
	return toString("General", `
Flags:
	`+padding("-e=<env>")+`Override the default environment (see -h=`+config.ConfigFileName+`)

The following helps are available:

	`+padding("-help or -h")+`General help
	`+padding("-h=select")+`SELECT to query a specific table
	`+padding("-h=insert")+`INSERT to add a new record into a table
	`+padding("-h=update")+`UPDATE to update one or more records in a table
	`+padding("-h=delete")+`DELETE to delete one or more records in a table

	`+padding("-h=alter")+`ALTER TABLE to modify a structure of table
	`+padding("-h=drop")+`DROP TABLE to drop a table and all its data

	`+padding("-h=table")+`TABLES to view the tables and schemas in database

	`+padding("-h="+config.ConfigFileName)+"Help with the "+config.ConfigFileName+` settings file

Notes:
`)
}
