package help

import "bt/config"

func displayConfigHelp() string {
	path, _ := config.FileName()
	return toString(config.ConfigFileName,
		`FileName to config file is: `+path+`

Da settings....looks something like this:

{
  "env": [
    {
      "name":           "dev",
      "default":        true,
      "project":        "*******",
      "instance":       "*******",
      "creds":          "",
      "admin-endpoint": "",
      "data-endpoint":  ""
    },....
  ],
  "tables": [
    {
      "name":           "msg_patrons",
      "keyPattern":     "com.sr#tenant#{{tid}}#patrons#{{pid}}"
    },....
  ],
  "defaults": {
    "displayCount": 1000
  }
}

Legend:
  {{ }} parameter

env:

tables:

defaults:
   `+padding("displayCount")+`Max number of records displayed before prompting user to continue

`)
}
