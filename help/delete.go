package help

func displayDeleteHelp() string {
	return toString("Select",
		`DELETE FROM {table} [WHERE {Family:Qualifier|param|prefix} {=|CONTAINS|IN({value}[, ....])} {value|@file} [AND ....]]

Legend:
   [ ]    Optional
   { }    Value

CONTAINS:
   Treated as a regex. An ".*" is added before and after the value.

IN:
   Queries for all values that equal the comma separated values.

      Example: DELETE FROM table WHERE tid = 283892 AND View:Id IN (19283, 21489, 48192)

   This example will query the big table named, "table", and delete all rows where the tid is 283892 and the View:Id 
   is equal to 19283, 21489 or 48192.

CONTAINS IN:

      Example: DELETE FROM table WHERE tid = 283892 AND View:html CONTAINS IN(smallRow, largeRow) 

   This example will query the big table named, "table", and delete all rows where the tid is 283892 and the View:html 
   contains .*smallRow.* or .*largeRow.*

Notes:
   The names of "Family:Qualifier" and "table" are case-sensitive. Also the values are case-sensitive. Keywords are not
   case-sensitive.
   
   `+padding("")+`Since "param" is use on the key, it only works with "=", does not supported with "contains"
   `+padding(`"prefix"`)+`Overrides any prefix defined by the table (see config). Must use "=", not "contains"
`)
}
