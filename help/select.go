package help

import (
	"bt/config"
	"bt/flag"
)

func displaySelectHelp() string {
	return toString("Select",
		`SELECT (*|(@file=)Family:Qualifier([option])...) FROM {table} (WHERE {Family:Qualifier|param|prefix} {=|CONTAINS|IN[{value}(, ....)]} {value|@file} (AND ....)) (limit {number})

Legend:
   ( )    Optional
   { }    Value

Flags:
   `+padding("-e")+`Specify the environment to use (i.e. "-o=prod" or "-o=stg") (see bt.json)

   `+padding("-fmt="+flag.FlagFormatHoriz)+`Format output horizontally. OS specifies console width. (default)
   `+padding("-fmt="+flag.FlagFormatHoriz+"[#]")+`Format output horizontally. # specifies console width.
   `+padding("-fmt="+flag.FlagFormatVert)+`Format output vertically
   `+padding("-fmt="+flag.FlagFormatJSON)+`Format output JSON
   `+padding("-fmt="+flag.FlagFormatHTML)+`Format output HTML and open the default browser

   `+padding("-code")+`Generated code from query
   `+padding("-o=fam:qual[options]")+`Sets the option for a specific column
   `+padding("-struct=s")+`All records have the same structure (same)
   `+padding("-struct=u")+`Rows displayed as found in the database (unique)
   `+padding("-ver")+`Show the column version
   `+padding("-width=#")+`Specifies the width of the console. Defaults to size Operating System reports. -1 for no resizing.

Option:
   Options are applied to a specific family and/or qualifier. Multiple options may be specified, separated by a pipe "|".
   They may be applied as a flag (i.e. "-o=fam:qual[options]" or to a specific column within the SELECT statement
   (i.e. "SELECT fam:qual[options] FROM ....". Options specified within the SELECT statement take precidence over those
   specified through a flag.

   The following options are supported:

   `+padding("asis")+`Do not apply any special formatting (default)
   `+padding("json")+`Display as JSON
   `+padding("hex")+`Hex dump column

   `+padding("auto")+`Automatically determine the width to best fit the width of the console (default)
   `+padding("full")+`Do not adjust the width
   `+padding("hide")+`Do not display
   `+padding("#")+`number representing the desired fixed width

Option Examples:
   `+padding("Customer:Meta[json]")+`Display "Customer:Meta"" as JSON
   `+padding("Customer:[json]")+`Display as every qualifier within the "Customer" family in JSON format
   `+padding(":Raw[hex]")+`Display as every qualifier named "Raw", regardless of the family, in hex format
   `+padding("Customer:Meta[40]")+`Display as "Customer:Meta" with a fixed with of 40 characters
   `+padding("Customer:Meta[json|100]")+`Display as "Customer:Meta"" with a fixed with of 100 characters, in JSON format
   `+padding("Customer:[hide]")+`Do not display the "Customer" family

REGEX:
   The filter values may be a valid RE2 Regular Expressions. See https://github.com/google/re2/wiki/Syntax
   for details.

CONTAINS:
   Treated as a regex. An ".*" is added before and after the value.

IN:
   Queries for all values that equal the comma separated values. The parentheses are optional.

      Example: SELECT FROM table WHERE tid = 283892 AND View:Id IN (19283, 21489, 48192)
                SELECT FROM table WHERE tid = 283892 AND View:Id IN 19283, 21489, 48192 AND View:Name IN Steve, Rita

   This example will query the big table named, "table", where the tid is 283892 and the View:Id is equal to
   19283, 21489 or 48192.

      Example: SELECT FROM table WHERE tid = 283892 AND View:Id IN 19283, 21489, 48192 AND View:Name IN Steve, Rita

   This example will query the big table named, "table", where the tid is 283892 and the View:Id is equal to
   19283, 21489 or 48192, and View:Name is Steve or Rita.

CONTAINS IN:

      Example: SELECT FROM table WHERE tid = 283892 AND View:html CONTAINS IN(smallRow, largeRow) 

   This example will query the big table named, "table", where the tid is 283892 and the View:html contains
   .*smallRow.* or .*largeRow.*

@file:
   Currently not supported....but soon

Notes:
   The names of "Family:Qualifier" and "table" are case-sensitive. Also the values are case-sensitive. Keywords are not
   case-sensitive.
   If no columns or "*" is specified, all columns (or "*") is assumed

   `+padding(`"param"`)+`Are defined in the `+config.ConfigFileName+` file (use "-h=`+config.ConfigFileName+`" for help with `+config.ConfigFileName+` file)
   `+padding("")+`Since "param" is use on the key, it only works with "=", does not supported with "CONTAINS"
   `+padding(`"prefix"`)+`Overrides any prefix defined by the table (see config). Must use "=", not "CONTAINS"
   `+padding(`"@file"`)+`Is the path with a file containing the value
`)
}
