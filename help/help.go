package help

import (
	"fmt"
	"os"
	"strings"

	"bt/config"
)

// Display will provide help to the console
func Display(help string) string {

	if help == "" {
		if len(os.Args) > 1 {
			help = os.Args[1]
		}
	}

	ret := ""
	switch strings.ToLower(help) {
	case "select":
		ret = displaySelectHelp()
	case "read":
		ret = displaySelectHelp()
	case config.ConfigFileName:
		ret = displayConfigHelp()
	case "table":
		ret = displayTableHelp()
	case "delete":
		ret = displayDeleteHelp()
	default:
		ret = displayGeneralHelp()
	}

	return ret
}

func toString(title, help string) string {
	return fmt.Sprintf("%s %s Help %s\n%s\n", strings.Repeat("-", 20), title, strings.Repeat("-", 20), help)
}

func padding(value string) string {
	return value + strings.Repeat(" ", 25-len(value))
}
