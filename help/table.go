package help

func displayTableHelp() string {
	return toString("TABLE",
		`TABLE {LIST|SCHEMA {table}|COUNT {table}}

Legend:
   { }    Value

Notes:
   Be careful about performing COUNT, because of performance reasons. It reads and counts
   each row, one-by-one.
`)
}
