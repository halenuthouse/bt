package main

import (
	"fmt"
	"log"
	"os"
	"strconv"

	"bt/browser"
	"bt/flag"
	"bt/help"
	"bt/server"

	"bt/execute"
)

func main() {

	// Check for server request
	if port, svr := flag.FindArg("s", "server"); svr {
		portNum, err := strconv.Atoi(port)
		if port != "" && err != nil {
			fmt.Println("Unable to determine server port")
			return
		}

		url, port, err := server.ExecStatementURL(portNum, os.Args[1:])
		if err != nil {
			log.Fatal(err)
		}

		// Launch the browser and server
		browser.Open(url)
		server.Launch(port)
		return
	}

	response, hlp, file2Browser, err := execute.Process(os.Stdout, os.Args[1:])
	if err != nil {
		fmt.Fprintf(os.Stderr, "%v\n", err)
	}

	if hlp != "" {
		fmt.Println(help.Display(hlp))
	}

	if file2Browser != "" {

		url, port, err := server.ExecBrowserURL()
		if err != nil {
			log.Fatal(err)
		}

		// Launch the browser and server
		browser.Open(url)
		server.LaunchBrowserFile(file2Browser, port)
	}

	fmt.Println(response)
}
