package gencode

import (
	"bytes"
	"fmt"
	"reflect"
	"strings"
	"text/template"

	"bt/statement"

	"bt/parser"

	"cloud.google.com/go/bigtable"
)

// Generate the code
func Generate(method, tableName, project, instance string, keyList bigtable.RowList, prefixList []bigtable.RowSet, famQualFilterCode string, sets []statement.SetField, limit int64) (string, error) {

	// Build code
	var actionFuncName string
	var actionFuncCode string
	var actionFuncParam string
	var actionImports string
	var err error

	switch method {
	case parser.UPDATE:
		actionFuncName = "updateRow"
		actionFuncParam = "ctx, tbl, row"
		actionFuncCode, err = buildUpdateActionCode(sets)
		if err != nil {
			return "", err
		}
	case parser.DELETE:
		actionFuncName = "deleteRow"
		actionFuncParam = "ctx, tbl, row.Key()"
		actionFuncCode = deleteRowCode
	case parser.READ:
		fallthrough
	default:
		actionFuncName = "printRow"
		actionFuncCode = printRowCode
		actionFuncParam = "row"
		actionImports = `	"sort"
	"strings"
	"time"`
	}

	rowFilterCode := buildFilterCode(famQualFilterCode)

	readCode, err := buildReadCode(actionFuncName, actionFuncParam, keyList, prefixList, limit)
	if err != nil {
		return "", err
	}

	// Build the final template
	data := struct {
		TableName     string
		Project       string
		Instance      string
		ActionCode    string
		ActionName    string
		ActionImports string
		ReadRecords   string
		RowFilterCode string
	}{
		TableName:     tableName,
		Project:       project,
		Instance:      instance,
		ActionCode:    actionFuncCode,
		ActionName:    actionFuncName,
		ActionImports: actionImports,
		ReadRecords:   readCode,
		RowFilterCode: rowFilterCode,
	}

	tmpl, err := template.New("main").Parse(main)
	if err != nil {
		return "", err
	}

	buf := new(bytes.Buffer)
	err = tmpl.Execute(buf, data)
	if err != nil {
		return "", err
	}

	return buf.String(), err
}

func buildUpdateActionCode(fields []statement.SetField) (string, error) {

	// Build mutations
	mutCode := ""
	for _, field := range fields {
		mutCode += prependTab(fmt.Sprintf(`mut.Set("%s", "%s", version, []byte("%s"))`, field.Family, field.Qualifier, field.Value), 1)
	}

	// Build code from template
	readData := struct {
		Mutations string
	}{Mutations: mutCode}

	tmpl, err := template.New("updateCode").Parse(updateRowCode)
	if err != nil {
		return "", err
	}

	buf := new(bytes.Buffer)
	err = tmpl.Execute(buf, readData)
	return buf.String(), err
}

func buildFilterCode(famQualFilterCode string) string {
	return fmt.Sprintf("\tfilter := bigtable.RowFilter(\n%s\t)", prependTabs(famQualFilterCode, 2))
}

func buildReadCode(actionFuncName, actionFuncParam string, keyList bigtable.RowList, prefixList []bigtable.RowSet, limit int64) (string, error) {
	readCode := ""

	var readCodeTemplate string
	readData := struct {
		PrimaryKey  string
		PrimaryKeys []string
		KeyPrefix   string
		ActionName  string
		ActionParam string
		Limit       int64
	}{
		ActionName:  actionFuncName,
		ActionParam: actionFuncParam,
		Limit: limit,
	}

	if len(keyList) > 0 {
		// Filter by primary key
		if len(keyList) == 1 {
			readData.PrimaryKey = keyList[0]
			readCodeTemplate = readSingleRecordCode
		} else {
			readData.PrimaryKeys = keyList
			readCodeTemplate = readMultiplePrimaryKeysCode
		}

		readTmpl, err := template.New("readCode").Parse(readCodeTemplate)
		if err != nil {
			return "", err
		}

		buf := new(bytes.Buffer)
		err = readTmpl.Execute(buf, readData)
		if err != nil {
			return "", err
		}

		readCode += buf.String()
	} else {
		// Filter by prefix
		for _, prefix := range prefixList {
			v := reflect.ValueOf(prefix)
			readData.KeyPrefix = v.FieldByName("start").String()
			readCodeTemplate = readPrefixRecordsCode

			readTmpl, err := template.New("readCode").Parse(readCodeTemplate)
			if err != nil {
				return "", err
			}

			buf := new(bytes.Buffer)
			err = readTmpl.Execute(buf, readData)
			if err != nil {
				return "", err
			}

			readCode += buf.String()
		}
	}

	return readCode, nil
}

func prependTabs(str string, indent int) string {
	split := strings.Split(str, "\n")
	ret := ""
	for _, line := range split {
		ret += strings.Repeat("\t", indent) + line + "\n"
	}
	return ret
}

func prependTab(line string, indent int) string {
	return fmt.Sprintf("%s%s\n", strings.Repeat("\t", indent), line)
}
