package gencode

const (
	main = `
package main

import (
	"cloud.google.com/go/bigtable"
	"fmt"
	"golang.org/x/net/context"
	"log"
{{.ActionImports}}
)

const (
	tableName = "{{.TableName}}"
	project   = "{{.Project}}"
	instance  = "{{.Instance}}"
)

func main() {

	// Open the client
	ctx := context.Background()
	client, err := bigtable.NewClient(ctx, project, instance)
	if err != nil {
		log.Fatalf("Could not create data operations client: %v", err)
	}
	defer client.Close()

	// Open the table
	tbl := client.Open(tableName)

	// Build Row Filter
{{.RowFilterCode}}
{{.ReadRecords}}
}

{{.ActionCode}}
`

	readPrefixRecordsCode = `
	// ********************
	// {{.KeyPrefix}}
	// ********************
	count := 0
	err = tbl.ReadRows(
		ctx,
		bigtable.PrefixRange("{{.KeyPrefix}}"),
		func(row bigtable.Row) bool {
			if err := {{.ActionName}}({{.ActionParam}}); err != nil {
				log.Panic(fmt.Sprintf("{{.ActionName}}: %v", err))
				return false
			}
			count++
			return true
		},
		filter{{if ne .Limit 0}},
		bigtable.LimitRows({{.Limit}}){{end}})
	if err != nil {
		log.Panic(fmt.Sprintf("reading: %v", err))
	}

	fmt.Printf("\nRecord Count: %d\n", count)
`

	readMultiplePrimaryKeysCode = `
	// ********************
	// Read all the keys
	// ********************
	keys := bigtable.RowList{
{{range .PrimaryKeys}}		"{{. }}",
{{end}}	}

	err = tbl.ReadRows(
		ctx,
		keys,
		func(row bigtable.Row) bool {
			if err := {{.ActionName}}({{.ActionParam}}); err != nil {
				log.Panic(fmt.Sprintf("{{.ActionName}}: %v", err))
				return false
			}
			return true
		},
		filter{{if eq .Limit 0}}{{else}},
		bigtable.LimitRows({{.Limit}}){{end}})
	if err != nil {
		log.Panic(fmt.Sprintf("reading: %v", err))
	}
`

	readSingleRecordCode = `
	// ********************
	// {{.PrimaryKey}}
	// ********************	
	row, err = tbl.ReadRow(
		ctx,
		"{{.PrimaryKey}}",
		filter,
	)

	if err != nil {
		log.Panic(fmt.Sprintf("reading: %v", err))
	}

	// If key is empty, then no record found
	if row.Key() != "" {
		if err = {{.ActionName}}({{.ActionParam}}); err != nil {
			log.Panic(fmt.Sprintf("{{.ActionName}}: %v", err))
		}
	}
`

	printRowCode = `// Sorting
type byColumn []bigtable.ReadItem

func (b byColumn) Len() int           { return len(b) }
func (b byColumn) Swap(i, j int)      { b[i], b[j] = b[j], b[i] }
func (b byColumn) Less(i, j int) bool { return b[i].Column < b[j].Column }

type byFamilyName []bigtable.FamilyInfo

func (b byFamilyName) Len() int           { return len(b) }
func (b byFamilyName) Swap(i, j int)      { b[i], b[j] = b[j], b[i] }
func (b byFamilyName) Less(i, j int) bool { return b[i].Name < b[j].Name }

// printRow will display to the console the bigtable.Row read from the database
func printRow(r bigtable.Row) error {
	fmt.Println(strings.Repeat("-", 40))
	fmt.Println(r.Key())

	var fams []string
	for fam := range r {
		fams = append(fams, fam)
	}
	sort.Strings(fams)
	for _, fam := range fams {
		ris := r[fam]
		sort.Sort(byColumn(ris))
		for _, ri := range ris {
			ts := time.Unix(0, int64(ri.Timestamp)*1e3)
			fmt.Printf("  %-40s @ %s\n", ri.Column, ts.Format("2006/01/02-15:04:05.000000"))
			fmt.Printf("    %q\n", ri.Value)
		}
	}

	return nil
}
`

	deleteRowCode = `
// deleteRow will delete a single bigtable.Row in the database
func deleteRow(ctx context.Context, tbl *bigtable.Table, key string) error {
	mut := bigtable.NewMutation()
	mut.DeleteRow()
	return tbl.Apply(ctx, key, mut)
}
`

	updateRowCode = `
// updateRow will update a single bigtable.Row in the database
func updateRow(ctx context.Context, tbl *bigtable.Table, r bigtable.Row) error {
	mut := bigtable.NewMutation()
	version := bigtable.Now()

{{.Mutations}}
	return tbl.Apply(ctx, r.Key(), mut)
}
`
)
