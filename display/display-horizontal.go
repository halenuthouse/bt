package display

import (
	"encoding/hex"
	"fmt"
	"hash/fnv"
	"sort"
	"strings"

	"bt/statement"
	"cloud.google.com/go/bigtable"
)

const defaultConsoleWidth = 160

var (
	headerTopLeft string
	headerFamilySeparator string
	headerTopRight string

	headerHorizSeparator string
	headerVertSeparator string

	rowHorizSeparator string
	rowHorizFamilySeparator string

	rowSeparatorLeft string
	rowSeparatorFamily string
	rowSeparatorColumn string
	rowSeparatorRight string

	bottomSeparatorLeft string
	bottomSeparatorFamily string
	bottomSeparatorColumn string
	bottomSeparatorRight string
)

var (
	colorBlack string
	colorRed string
	colorGreen string
	colorYellow string
	colorBlue string
	colorMagenta string
	colorCyan string
	colorWhite string
	colorReset string

	styleUnderline string
	styleBold      string
)

func (info *Info) initializeHorizontal(options string) {
	info.Cleanup = info.cleanupHorizontal

	info.PrintRow = info.printOrCacheRow
	info.printRow = info.printRowHorizontal

	info.printSeparator = func() {
		info.printVerticalSeparator(bottomSeparatorLeft, bottomSeparatorFamily, bottomSeparatorColumn, bottomSeparatorRight)
	}

	info.initializeCache(options)

	if info.supportAnsi {
		headerTopLeft = "┏"
		headerFamilySeparator = "┳"
		headerTopRight = "┓"

		headerHorizSeparator = "━"
		headerVertSeparator = "┃"

		rowHorizSeparator = "│"
		rowHorizFamilySeparator = "┃"

		rowSeparatorLeft = "┣"
		rowSeparatorFamily = "╋"
		rowSeparatorColumn = "┿"
		rowSeparatorRight = "┫"

		bottomSeparatorLeft = "┗"
		bottomSeparatorFamily = "┻"
		bottomSeparatorColumn = "┷"
		bottomSeparatorRight = "┛"

		colorBlack = "\u001b[30m"
		colorRed = "\u001b[31m"
		colorGreen = "\u001b[32m"
		colorYellow = "\u001b[33m"
		colorBlue = "\u001b[34m"
		colorMagenta = "\u001b[35m"
		colorCyan = "\u001b[36m"
		colorWhite = "\u001b[37m"
		colorReset = "\u001b[0m"

		styleUnderline = "\u001b[4m"
		styleBold = "\u001b[1m"
	} else {
		headerTopLeft = "+"
		headerFamilySeparator = "+"
		headerTopRight = "+"

		headerHorizSeparator = "-"
		headerVertSeparator = "|"

		rowHorizSeparator = "|"
		rowHorizFamilySeparator = "|"

		rowSeparatorLeft = "+"
		rowSeparatorFamily = "+"
		rowSeparatorColumn = "+"
		rowSeparatorRight = "+"

		bottomSeparatorLeft = "+"
		bottomSeparatorFamily = "+"
		bottomSeparatorColumn = "+"
		bottomSeparatorRight = "+"
	}
}

func (info *Info) cleanupHorizontal() string {
	info.flush()
	if info.recordCount+info.cachedRecordCount > 0 {
		info.printVerticalSeparator(bottomSeparatorLeft, bottomSeparatorFamily, bottomSeparatorColumn, bottomSeparatorRight)
	}

	fmt.Fprintln(info.out, "\nTotal Records:", info.recordCount)
	if len(info.trimmedColumns) > 0 {
		list := []string{}
		for key := range info.trimmedColumns {
			list = append(list, fmt.Sprintf("%s[%d]", key, info.trimmedColumns[key]))
		}
		sort.Strings(list)
		fmt.Fprintln(info.out, "Trimmed:", strings.Join(list, ","))
	}

	return ""
}

func (info *Info) printRowHorizontal(key string, record Record) {
	if !info.headerPrinted {
		info.printHorizontalHeader()
	}

	info.printLine(fmt.Sprintf("KEY: %s", key), styleUnderline)

	fmt.Fprint(info.out, rowHorizFamilySeparator)

	var errs []error
	for _, f := range info.displayFamilies {
		for qi, q := range f.qualifiers {
			if q.option.Format != statement.HEX {
				value, err := formatColumnForDisplay(f.family, q.qualifier, record, q.option)
				if err != nil {
					errs = append(errs, err)
				}
				if PrintLeftJustified(info.out, string(value), q.dispWidth) {
					colName := f.family+":"+q.qualifier
					info.trimmedColumns[colName] = q.width
				}
				if qi < len(f.qualifiers)-1 {
					fmt.Fprint(info.out, rowHorizSeparator)
				} else {
					fmt.Fprint(info.out, rowHorizFamilySeparator)
				}
			}
		}
	}
	fmt.Fprintln(info.out, )

	// Display any hex dumps
	for _, f := range info.displayFamilies {
		for _, q := range f.qualifiers {
			if q.option.Format == statement.HEX {
				colName := f.family + ":" + q.qualifier
				value := record[colName].Value

				if len(value) == 0 {
					continue
				}

				lines := strings.Split(hex.Dump([]byte(value)), "\n")
				info.printLine(colName + strings.Repeat(" ", len(lines[0])-len(colName)), styleBold)
				for _, line := range lines {
					info.printLine(line, "")
				}
			}
		}
	}

	for _, err := range errs {
		info.printLine(err.Error(), colorRed)
	}

	info.recordCount++
}

func (info *Info) printLine(message string, color string) {
	msg := []rune(message)
	line := []rune(info.blankLine())

	curColor := colorReset
	out := []rune{}
	for pos := 0; pos<max(len(msg), len(line)); pos++ {
		if pos > 0 && pos <= len(msg) {
			if curColor != color {
				curColor = color
				out = append(out, []rune(curColor)...)
			}
			out = append(out, msg[pos-1])
		} else {
			if curColor != colorReset {
				curColor = colorReset
				out = append(out, []rune(curColor)...)
			}
			out = append(out, line[pos])
		}
	}
	fmt.Fprintln(info.out, string(out)+colorReset)
}

func (info *Info) blankLine() string {
	line := rowHorizFamilySeparator
	for _, f := range info.displayFamilies {
		for qi, q := range f.qualifiers {
			if q.option.Format != statement.HEX {
				line += strings.Repeat(" ", q.dispWidth)
				if qi < len(f.qualifiers)-1 {
					line += rowHorizSeparator
				} else {
					line += rowHorizFamilySeparator
				}
			}
		}
	}
	return line
}

func (info *Info) printHorizontalHeader() {

	info.printVerticalSeparator(headerTopLeft, headerFamilySeparator, headerHorizSeparator, headerTopRight)

	// Print the Family line
	fmt.Fprint(info.out, headerVertSeparator)
	for _, f := range info.displayFamilies {
		PrintCentered(info.out, f.family, f.familyWidth)
		fmt.Fprint(info.out, headerVertSeparator)
	}
	fmt.Fprintln(info.out, )

	// Print the Qualifier line
	fmt.Fprint(info.out, rowHorizFamilySeparator)
	for fi := range info.displayFamilies {
		for qi, q := range info.displayFamilies[fi].qualifiers {
			if q.option.Format != statement.HEX {
				colName := info.displayFamilies[fi].family+":"+q.qualifier
				if PrintCentered(info.out, q.qualifier, q.dispWidth) {
					width, ok := info.trimmedColumns[colName]
					if !ok {
						width = q.dispWidth
					}
					info.trimmedColumns[colName] = max(len(colName), width)
				}
				if qi == len(info.displayFamilies[fi].qualifiers)-1 {
					fmt.Fprint(info.out, rowHorizFamilySeparator)
				} else {
					fmt.Fprint(info.out, rowHorizSeparator)
				}
			}
		}
	}
	fmt.Fprintln(info.out, )

	info.printVerticalSeparator(rowSeparatorLeft, rowSeparatorFamily, rowSeparatorColumn, rowSeparatorRight)
	info.headerPrinted = true
}

func (info *Info) printVerticalSeparator(left, famSep, colSep, right string) {

	fmt.Fprint(info.out, left)
	for fi := range info.displayFamilies {
		for qi := range info.displayFamilies[fi].qualifiers {
			if info.displayFamilies[fi].qualifiers[qi].option.Format != statement.HEX {
				fmt.Fprint(info.out, strings.Repeat(headerHorizSeparator, info.displayFamilies[fi].qualifiers[qi].dispWidth))
				if qi == len(info.displayFamilies[fi].qualifiers)-1 {
					if fi == len(info.displayFamilies)-1 {
						fmt.Fprint(info.out, right)
					} else {
						fmt.Fprint(info.out, famSep)
					}
				} else {
					fmt.Fprint(info.out, colSep)
				}
			}
		}
	}
	fmt.Fprintln(info.out)
}

func (info *Info) calcHashForRowStructure(row bigtable.Row) uint32 {
	columnList := []string{}
	for fam := range row {
		for _, readItem := range row[fam] {
			columnList = append(columnList, readItem.Column)
		}
	}

	sort.Strings(columnList)

	h := fnv.New32a()
	h.Write([]byte(strings.Join(columnList, "|")))
	return h.Sum32()

}

func (info *Info) totalUpWidths() {
	info.totalColWidth = 0
	for fi := range info.displayFamilies {
		info.displayFamilies[fi].familyWidth = -1
		for _, q := range info.displayFamilies[fi].qualifiers {
			if q.option.Format == statement.Hide || q.option.Format == statement.HEX {
				continue
			}

			info.displayFamilies[fi].familyWidth += q.dispWidth + 1
			info.totalColWidth += q.dispWidth + 1 // the +1 is to account for separator
		}
	}

	info.totalColWidth += 2
}
