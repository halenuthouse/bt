package display

import (
	"encoding/json"
	"fmt"
	"os"
	"sort"
	"strings"

	"bt/flag"
	"bt/statement"
	"cloud.google.com/go/bigtable"
)

const (
	unprintableChar = "▌"
	cutStringChar   = "┄"
	versionFormat   = "2006/01/02-15:04:05.000000"
)

type (
	mapDispMap struct {
		column statement.Column
		option statement.ColumnOption
		width  int
	}

	queryDisplayMapping struct {
		field   *statement.Field
		display []mapDispMap
	}

	displayQualifier struct {
		qualifier string
		option    statement.ColumnOption
		dispWidth int
		width     int
	}

	displayFamily struct {
		family      string
		familyWidth int

		qualifiers []displayQualifier
	}

	// Info struct holds the data required to print rows in the user specified format
	Info struct {
		sameRowStruct bool
		showVersion   bool
		headerPrinted bool
		recordCount   int
		statementRaw  string

		out      *os.File

		supportAnsi bool

		queryFields []statement.Field

		fieldMapping []queryDisplayMapping

		displayFamilies []displayFamily

		columnOptions map[statement.Column]statement.ColumnOption

		cacheInfo

		// Display Functions
		PrintRow func(row bigtable.Row) bool
		Cleanup  func() string
	}

	// Cell contains the bigtable cell information
	Cell struct {
		Value   []byte
		Version string
	}

	// Record is a map of a bigtable row
	Record map[string]Cell
)

func New(stmt *statement.Statement, flags flag.Flags, out *os.File) *Info {
	info := Info{
		showVersion:   flags.ShowColumnVersion(),
		sameRowStruct: flags.SameRowStruct(),
		headerPrinted: false,
		supportAnsi:   true, //(!isatty.IsTerminal(os.Stdout.Fd()) && !isatty.IsCygwinTerminal(os.Stdout.Fd())),
		out:           out,
	}

	info.statementRaw = stmt.Raw
	info.queryFields = stmt.Fields

	info.columnOptions = map[statement.Column]statement.ColumnOption{}
	for _, value := range flags.ColumnOptions() {
		column := statement.ParseColumn(value)
		info.columnOptions[column.Column] = *column.Options
	}

	format, formatOption := flags.Format()
	switch format {
	case flag.FlagFormatVert:
		info.initializeVertical(formatOption)
	case flag.FlagFormatJSON:
		info.initializeJSON(formatOption)
	case flag.FlagFormatHTML:
		info.initializeHTML(formatOption)
	default:
		info.initializeHorizontal(formatOption)
	}

	if flags.Debug() {
		fmt.Fprintf(info.out, "Console Width: %d\n", info.outputWidth)
	}
	return &info
}

// PrintCentered adds padding to the beginning and end of text to center it
func PrintCentered(out *os.File, text string, width int) bool {
	if width == 0 {
		return true
	}

	trimmed := false
	if len(text) > width {
		text = text[:width-1] + cutStringChar
		trimmed = true
	}

	padding := width/2 - len(text)/2
	if padding < 0 {
		padding = 0
	}
	right := width - padding - len(text)
	if right < 0 {
		right = 0
	}
	fmt.Fprint(out, strings.Repeat(" ", padding))
	fmt.Fprint(out, cleanedText(text))
	fmt.Fprint(out, strings.Repeat(" ", right))
	return trimmed
}

// PrintLeftJustified add padding to the end of the text
func PrintLeftJustified(out *os.File, text string, width int) bool {
	if width == 0 {
		return true
	}

	trimmed := false
	if len(text) > width {
		text = text[:width-1] + cutStringChar
		trimmed = true
	}

	padding := width - len(text)
	if padding < 0 {
		padding = 0
	}
	fmt.Fprint(out, cleanedText(text))
	fmt.Fprint(out, strings.Repeat(" ", padding))
	return trimmed
}

func (info *Info) findColumnOption(column statement.Column) *statement.ColumnOption {
	for _, col := range []statement.Column{column, {Family: column.Family}, {Qualifier: column.Qualifier}} {
		options, found := info.columnOptions[col]
		if found {
			return &options
		}
	}
	return &statement.ColumnOption{}
}

func cleanedText(text string) string {
	ret := ""
	for _, ch := range text {
		if ch < ' ' {
			ret += unprintableChar
		} else {
			ret += string(ch)
		}
	}
	return ret
}

func formatColumnForDisplay(family, qualifier string, record Record, option statement.ColumnOption) ([]byte, error) {
	colName := family + ":" + qualifier
	value := record[colName].Value
	if option.Format == statement.JSON && len(value) > 0 {
		if err := json.Unmarshal(value, &value); err != nil {
			return value, fmt.Errorf(`not valid JSON "%s": %v`, colName, err)
		}
	}
	return value, nil
}

func (info *Info) prepareForDisplay() {

	// Sort the display columns
	for i := range info.fieldMapping {
		sort.Slice(info.fieldMapping[i].display,
			func(first, second int) bool {
				return info.fieldMapping[i].display[first].column.Name() < info.fieldMapping[i].display[second].column.Name()
			},
		)
	}

	// Build display families
	info.displayFamilies = []displayFamily{}
	curName := ""

	for _, fm := range info.fieldMapping {
		for _, disp := range fm.display {
			if disp.option.Format == statement.Hide {
				continue
			}

			colWidth := disp.option.Size
			if  disp.option.Format == statement.HEX {
				colWidth = 0
			} else if colWidth == 0 {
				colWidth = max(len(disp.column.Qualifier), disp.width)
			}

			if curName != disp.column.Family {
				curName = disp.column.Family
				info.displayFamilies = append(info.displayFamilies, displayFamily{family: disp.column.Family, familyWidth: -1})
			}
			lastDispIndex := len(info.displayFamilies) - 1
			info.displayFamilies[lastDispIndex].qualifiers = append(
				info.displayFamilies[len(info.displayFamilies)-1].qualifiers,
				displayQualifier{disp.column.Qualifier, disp.option, colWidth, colWidth})
		}
	}

	info.totalUpWidths()
}

func max(first, second int) int {
	if first > second {
		return first
	}
	return second
}
