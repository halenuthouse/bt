package display

import (
	"bt/statement"
	"strconv"
	"testing"
)

type testDefinition struct {
	name          string
	consoleWidth  int
	totalWidth    int
	displayFamily []displayFamily
	expWidths     []int
}

func TestResize(t *testing.T) {
	tests := []testDefinition{
		allColumnsVeryWide(),
		fixedColumns(),
		fixedColumnsOneVeryWide(),
	}

	for _, test := range tests {
		t.Log("Running test:", test.name)
		info := Info{}
		info.initializeHorizontal(strconv.Itoa(test.consoleWidth))
		info.displayFamilies = test.displayFamily

		info.totalUpWidths()
		info.resizeColumnsToFit()

		expTotalWidth := info.totalColWidth
		if test.totalWidth != 0 {
			expTotalWidth = test.consoleWidth
		}
		if info.totalColWidth != expTotalWidth {
			t.Errorf("total width actual:%d expected %d", info.totalColWidth, expTotalWidth)
		}

		i := 0
		for _, f := range info.displayFamilies {
			for _, q := range f.qualifiers {
				if i >= len(test.expWidths) {
					t.Errorf("%s:%s more actual widths than expected (check defined tests)", f.family, q.qualifier)
				}
				if q.dispWidth != test.expWidths[i] {
					t.Errorf("%s:%s actual:%d expected:%d", f.family, q.qualifier, q.dispWidth, test.expWidths[i])
				}
				i++
			}
		}
	}
}

func fixedColumnsOneVeryWide() testDefinition {
	return testDefinition {
		name: "Fixed Columns, one very wide",
		consoleWidth: 120,
		displayFamily: []displayFamily{
			{
				family: "fam1",
				qualifiers: []displayQualifier{
					{
						qualifier: "qual1",
						dispWidth: 50,
						option:    statement.ColumnOption{Size:50},
					},
				},
			},
			{
				family: "fam2",
				qualifiers: []displayQualifier{
					{
						qualifier: "qual2",
						dispWidth: 10000,
						option:    statement.ColumnOption{},
					},
					{
						qualifier: "qual3",
						dispWidth: 50,
						option:    statement.ColumnOption{Format: "full"},
					},
				},
			},
		},
		expWidths: []int{50, 15, 50},
	}
}

func fixedColumns() testDefinition {
	return testDefinition {
		name: "Fixed Columns",
		consoleWidth: 120,
		displayFamily: []displayFamily{
			{
				family: "fam1",
				qualifiers: []displayQualifier{
					{
						qualifier: "qual1",
						dispWidth: 50,
						option:    statement.ColumnOption{Size:50},
					},
				},
			},
			{
				family: "fam2",
				qualifiers: []displayQualifier{
					{
						qualifier: "qual2",
						dispWidth: 100,
						option:    statement.ColumnOption{},
					},
					{
						qualifier: "qual3",
						dispWidth: 50,
						option:    statement.ColumnOption{Format: "full"},
					},
				},
			},
		},
		expWidths: []int{50, 15, 50},
	}
}

func allColumnsVeryWide() testDefinition {
	return testDefinition {
		name: "All Columns Very Wide",
		consoleWidth: 120,
			displayFamily: []displayFamily{
			{
				family: "fam1",
				qualifiers: []displayQualifier{
					{
						qualifier: "qual1",
						dispWidth: 400,
						option:    statement.ColumnOption{},
					},
				},
			},
			{
				family: "fam2",
				qualifiers: []displayQualifier{
					{
						qualifier: "qual2",
						dispWidth: 50000,
						option:    statement.ColumnOption{},
					},
				},
			},
		},
		expWidths: []int{5, 111},
	}
}