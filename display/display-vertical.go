package display

import (
	"bt/statement"
	"fmt"
	"sort"
	"strings"
	"time"

	"cloud.google.com/go/bigtable"
)

func (info *Info) initializeVertical(_ string) {
	info.PrintRow = info.printRowVertical
	info.Cleanup = info.cleanupVertical
}

func (info *Info) printRowVertical(row bigtable.Row) bool {
	fmt.Fprintln(info.out, strings.Repeat("-", 40))
	fmt.Fprintln(info.out, row.Key())

	var fams []string
	for fam := range row {
		fams = append(fams, fam)
	}

	maxWidth := 0
	if !info.showVersion {
		for _, fam := range row {
			for _, ri := range fam {
				maxWidth = max(maxWidth, len(ri.Column))
			}
		}
	}
	sort.Strings(fams)
	for _, fam := range fams {
		ris := row[fam]
		sort.Sort(byColumn(ris))

		for _, ri := range ris {
			option := info.findColumnOption(statement.ParseColumn(ri.Column).Column)
			switch option.Format {
			case statement.Hide:
				// do nothing
			case statement.HEX:
				// todo
			default:
				if info.showVersion {
					ts := time.Unix(0, int64(ri.Timestamp)*1e3)
					fmt.Fprintf(info.out, "  %-40s @ %s\n", ri.Column, ts.Format("2006/01/02-15:04:05.000000"))
					fmt.Fprintf(info.out,"    %q\n", ri.Value)
				} else {
					format := fmt.Sprintf("  %%-%ds %%q\n", maxWidth+1)
					fmt.Fprintf(info.out, format, ri.Column+":", ri.Value)
				}
			}
		}
	}
	info.recordCount++
	return true
}

func (info *Info) cleanupVertical() string {
	fmt.Fprintln(info.out,"\nTotal Records:", info.recordCount)
	return ""
}

