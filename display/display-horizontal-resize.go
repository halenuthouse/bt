package display

import (
	"sort"
)

type resizeScratch struct {
	dq      *displayQualifier
	percent float64
	width   int
	resized bool
}

func (info *Info) resizeColumnsToFit() {

	if info.outputWidth < 0 || info.totalColWidth <= info.outputWidth {
		return
	}

	scratch := info.buildScratch()

	// Resize Logic: we want to resize as few columns as possible. So we try to shrink the
	// largest family:qualifiers first
	newWidth := info.totalColWidth + resizeLargeColumns(info.outputWidth, scratch)
	for x := 0; x<4; x++ {
		if newWidth <= info.outputWidth {
			break
		}
		for i := range scratch {
			if newWidth <= info.outputWidth {
				break
			}

			scratch[i].percent = float64(scratch[i].width) / float64(newWidth)

			adj := scratch[i].width / 2
			if newWidth-adj < info.outputWidth {
				adj = newWidth - info.outputWidth
			}

			if adj != 0 {
				scratch[i].width -= adj
				newWidth -= adj
				scratch[i].resized = true
			}

		}
	}

	for _, s := range scratch {
		if s.resized {
			s.dq.dispWidth = s.width
		}
	}
	info.totalUpWidths()
}

func resizeLargeColumns(consoleWidth int, scratch []resizeScratch) int {
	newWidth := 0
	for i := range scratch {
		if scratch[i].width > consoleWidth {
			w := int(float64(consoleWidth) * scratch[i].percent)
			if w < len(scratch[i].dq.qualifier) {
				w = len(scratch[i].dq.qualifier)
			}
			adj := scratch[i].width - w
			if adj != 0 {
				scratch[i].width -= adj
				newWidth -= adj
				scratch[i].resized = true
			}
		}
	}

	return newWidth
}

func (info *Info) buildScratch() (scratch []resizeScratch) {
	for fi := range info.displayFamilies {
		for qi := range info.displayFamilies[fi].qualifiers {
			qualifiers := &info.displayFamilies[fi].qualifiers[qi]
			if qualifiers.option.Size != 0 || qualifiers.option.Format == "full" {
				continue
			}
			rs := resizeScratch{
				dq: qualifiers,
				percent: float64(info.displayFamilies[fi].qualifiers[qi].dispWidth) / float64(info.totalColWidth),
				width: info.displayFamilies[fi].qualifiers[qi].dispWidth,
			}
			scratch = append(scratch, rs)
		}
	}

	// Sort so that we process the largest columns first
	sort.Slice(scratch,
		func(first, second int) bool {
			return scratch[first].percent > scratch[second].percent
		},
	)

	return
}
