// +build linux darwin

package display

import (
	"syscall"
	"unsafe"
)

func getConsoleWidth() int {
	ws := &struct {
		Row    uint16
		Col    uint16
		Xpixel uint16
		Ypixel uint16
	}{}
	retCode, _, _ := syscall.Syscall(syscall.SYS_IOCTL,
		uintptr(syscall.Stdin),
		uintptr(syscall.TIOCGWINSZ),
		uintptr(unsafe.Pointer(ws)))

	if int(retCode) == -1 {
		return defaultConsoleWidth
	}
	return int(ws.Col)
}
