package display

import (
	"bt/display/html-templates"
	"bt/statement"
	"encoding/hex"
	"fmt"
	"html/template"
	"io/ioutil"
	"strconv"
	"strings"
)

const (
	widthOfChar = 6
)

func (info *Info) initializeHTML(options string) {
	info.Cleanup = info.cleanupHTML

	info.PrintRow = info.printOrCacheRow
	info.printRow = info.printRowHTML

	info.printSeparator = func() {}

	// Open the html file
	out, err := ioutil.TempFile("","bt-*.html")
	if err != nil {
		fmt.Println(err)
		return
	}

	info.out = out

	fmt.Fprint(info.out, html_templates.MainTemplateTop)

	// Initialize the cache
	info.initializeCache(options)
	info.outputWidth = 2000 / widthOfChar
}

func (info *Info) renderTemplate(name, html string, data interface{}) {
	tmpl, err := template.New(name).Parse(html)
	if err != nil {
		fmt.Println(err)
		return
	}

	if err = tmpl.Execute(info.out, data); err != nil {
		fmt.Println(err)
		return
	}
}

func (info *Info) printRowHTML(key string, record Record) {
	if !info.headerPrinted {
		info.printHTMLHeader()
	}

	var className string
	if info.recordCount % 2 == 0 {
		className = "evenRow"
	} else {
		className = "oddRow"
	}

	info.printHTMLLine(fmt.Sprintf("KEY: %s", key), className+" key")
	info.renderTemplate("printRow", html_templates.StartRow, className)

	var errs []error
	for _, f := range info.displayFamilies {
		for _, q := range f.qualifiers {
			if q.option.Format == statement.HEX {
				continue
			}

			value, err := formatColumnForDisplay(f.family, q.qualifier, record, q.option)
			if err != nil {
				errs = append(errs, err)
			}
			info.renderTemplate(
				"htmlRow",
				html_templates.Column,
				map[string]interface{} {
					"width": strconv.Itoa(q.dispWidth * widthOfChar),
					"value": string(value),
				})
		}
	}
	fmt.Fprintln(info.out, html_templates.EndRow)

	// Display any hex dumps
	r := strings.NewReplacer(" ", "&nbsp;", "\n", "<br/>")
	for _, f := range info.displayFamilies {
		for _, q := range f.qualifiers {
			if q.option.Format == statement.HEX {
				colName := f.family + ":" + q.qualifier
				value := record[colName].Value

				if len(value) == 0 {
					continue
				}

				lines := r.Replace(hex.Dump([]byte(value)))
				info.printHTMLLineSafe(colName+"<br/>"+lines, className+" hexDump")
			}
		}
	}

	for _, err := range errs {
		info.printHTMLLine(err.Error(), "error")
	}

	info.recordCount++
}

func (info *Info) printHTMLLine(line, className string) {

	span := 0
	for _, f := range info.displayFamilies {
		span += len(f.qualifiers)
	}

	info.renderTemplate(
		"printHTMLLine",
		html_templates.Row,
		map[string]interface{} {
			"value": line,
			"span": strconv.Itoa(span),
			"className": className,
		})
}

func (info *Info) spanAll() int {
	span := 0
	for _, f := range info.displayFamilies {
		span += len(f.qualifiers)
	}
	return span
}

func (info *Info) printHTMLLineSafe(line, className string) {
	span := info.spanAll()
	info.renderTemplate(
		"printHTMLLine",
		html_templates.Row,
		map[string]interface{} {
			"value": template.HTML(line),
			"span": strconv.Itoa(span),
			"className": className,
		})
}

func (info *Info) printHTMLHeader() {
	fmt.Fprintf(info.out, html_templates.StartHeader)

	span := info.spanAll()
	info.renderTemplate("statement", html_templates.Statement, map[string]interface{}{"span": strconv.Itoa(span), "stmtRaw": info.statementRaw})

	// Family
	fmt.Fprint(info.out, html_templates.FamilyHeaderRow)
	for _, f := range info.displayFamilies {
		info.renderTemplate("family", html_templates.FamilyHeaderCol, map[string]interface{}{"span": strconv.Itoa(len(f.qualifiers)), "value": f.family})
	}
	fmt.Fprint(info.out, html_templates.EndRow)

	// Qualifier
	fmt.Fprint(info.out, html_templates.QualifierHeaderRow)
	for _, f := range info.displayFamilies {
		for _, q := range f.qualifiers {
			info.renderTemplate("qual", html_templates.QualifierHeaderCol, map[string]interface{}{"value": q.qualifier})
		}
	}
	fmt.Fprint(info.out, html_templates.EndRow)

	// All done
	fmt.Fprint(info.out, html_templates.EndHeader)
	info.headerPrinted = true
}

func (info *Info) cleanupHTML() string {
	info.flush()
	fmt.Fprint(info.out, html_templates.EndTable)
	fmt.Fprintf(info.out, "\n<p>Total Records: %d</p>\n", info.recordCount)
	fmt.Fprint(info.out, html_templates.MainTemplateBottom)
	info.out.Close()

	return info.out.Name()
}
