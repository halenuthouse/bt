package display

import (
	"cloud.google.com/go/bigtable"
	"strconv"
)

type cacheInfo struct {
	records           map[string]Record
	totalColWidth     int
	caching           bool
	cachedRecordCount int
	structHash        uint32
	outputWidth       int
	trimmedColumns    map[string]int

	printRow func(key string, record Record)
	printSeparator func()
}

func (info *Info) initializeCache(options string) {
	info.caching = true
	info.records = map[string]Record{}
	info.trimmedColumns = map[string]int{}

	if options != "" {
		width, err := strconv.Atoi(options)
		if err != nil {
			info.outputWidth = -1
		} else {
			info.outputWidth = width
		}
	} else {
		info.outputWidth = getConsoleWidth()
	}
}

// flush will print the header, print the cache and empty it
func (info *Info) flush() {
	if info.recordCount > 0 {
		info.printSeparator()
	}

	info.prepareForDisplay()

	// Adjust widths of columns to fit console
	info.resizeColumnsToFit()

	for key, record := range info.records {
		info.printRow(key, record)
	}

	info.cachedRecordCount = 0
	info.records = map[string]Record{}
	info.headerPrinted = false
}

func (info *Info) printOrCacheRow(row bigtable.Row) bool {
	if !info.sameRowStruct {
		structHash := info.calcHashForRowStructure(row)
		if info.structHash != structHash {
			info.structHash = structHash
			info.flush()
			info.caching = true
		}
	}

	key, record := info.rowToRecord(row)
	if info.caching {
		if info.cachedRecordCount <= 1000 {
			info.records[row.Key()] = record
			info.cachedRecordCount++
			return true
		}

		info.flush()
		info.caching = false
	}

	info.printRow(key, record)
	return true
}

