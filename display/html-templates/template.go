package html_templates

const MainTemplateTop = `<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
		table.tbl {
            border: 1px solid #1C6EA4;
            background-color: #EEEEEE;
            text-align: left;
            border-collapse: collapse;
			width: 100%;
        }
        table.tbl td, table.tbl th {
            border: 1px solid #AAAAAA;
            padding: 1px 1px;
            overflow: hidden;
            text-overflow: ellipsis;
        }
        table.tbl tbody td {
            font-size: 11px;
        }

        table.tbl tr th, table.tbl tr td {
            vertical-align: top;
            text-overflow: ellipsis;
        }

        table.tbl thead .statement {
            font-size: 12px;
            color: #FFFFFF;
            text-align: left;
            border-left: 2px solid #D0E4F5;
        }

        table.tbl thead .famRow {
            font-size: 12px;
            font-weight: bold;
            color: #FFFFFF;
            text-align: center;
            border-left: 2px solid #D0E4F5;
            background: #1C6EA4;
            background: -moz-linear-gradient(top, #5592bb 0%, #327cad 66%, #1C6EA4 100%);
            background: -webkit-linear-gradient(top, #5592bb 0%, #327cad 66%, #1C6EA4 100%);
            background: linear-gradient(to bottom, #5592bb 0%, #327cad 66%, #1C6EA4 100%);
        }

        table.tbl thead .qualRow {
            font-size: 12px;
            font-weight: bold;
            color: #FFFFFF;
            text-align: center;
            border-left: 2px solid #D0E4F5;
            background: #1C6EA4;
            background: -moz-linear-gradient(top, #5592bb 0%, #327cad 66%, #1C6EA4 100%);
            background: -webkit-linear-gradient(top, #5592bb 0%, #327cad 66%, #1C6EA4 100%);
            background: linear-gradient(to bottom, #5592bb 0%, #327cad 66%, #1C6EA4 100%);
            border-bottom: 2px solid #444444;
        }

        .evenRow {
			background-color: #EEEEEE;
        }

        .oddRow {
			background: #D0E4F5;
        }

		.key {
			margin-top: 10px;
			font-weight: bolder;
		}

		.hexDump {
			font-family: monospace;
		}

		.error {
			color: red;
		}
	</style>
</head>
<body>
`

const StartHeader = "<table class='tbl'>\n\t<thead>\n"
const Statement = "<tr class='statement' style='background: black'><th colspan='{{.span}}'>{{.stmtRaw}}</th></tr>"
const EndHeader = "\t</thead>\n\t<tbody>\n"
const EndTable = "\t</tbody>\n</table>\n"

const StartRow = "\t<tr class='{{.}}'>\n"
const EndRow = "\t</tr>\n"

const Row = "\t<tr class='{{.className}}'><td colspan='{{.span}}'>{{.value}}</td><tr>\n"
const Column = "\t\t<td width='{{.width}}px'>{{.value}}</td>\n"

const FamilyHeaderRow = "\t<tr class='famRow'>\n"
const FamilyHeaderCol = "\t\t<th colspan='{{.span}}'>{{.value}}</th>\n"

const QualifierHeaderRow = "\t<tr class='qualRow'>\n"
const QualifierHeaderCol = "\t\t<th>{{.value}}</th>\n"

const MainTemplateBottom = `
</body>
<script>
    (function () {
        var colElm;
        var rowElm;
        var startOffset;

        Array.prototype.forEach.call(
            document.querySelectorAll("table td"),
            function (td) {
                makeColResizable(td);
                makeRowResizable(td);
            });

        Array.prototype.forEach.call(
            document.querySelectorAll("table th"),
            function (th) {
                makeColResizable(th);
            });

        document.addEventListener('mousemove', function (e) {
            if (colElm) {
                colElm.style.width = startOffset + e.pageX + 'px';
            }
            if (rowElm) {
                rowElm.style.height = startOffset + e.pageY + 'px';
            }
        });

        document.addEventListener('mouseup', function () {
            colElm = undefined;
            rowElm = undefined;
        });

        function createColResizeElement() {
            var grip = document.createElement('div');
            grip.innerHTML = "&nbsp;";
            grip.style.top = 0;
            grip.style.right = 0;
            grip.style.bottom = 0;
            grip.style.width = '3px';
            grip.style.position = 'absolute';
            grip.style.cursor = 'col-resize';
            return grip;
        }

        function makeColResizable(el) {
            el.style.position = 'relative';

            var grip = createColResizeElement();
            grip.addEventListener('mousedown', function (e) {
                colElm = el;
                startOffset = el.offsetWidth - e.pageX;
            });

            el.appendChild(grip);
        }

        function createRowResizeElement() {
            var grip = document.createElement('div');
            grip.style.bottom = 0;
            grip.style.width = '100%';
            grip.style.height = '3px';
            grip.style.position = 'absolute';
            grip.style.cursor = 'row-resize';
            return grip;
        }

        function makeRowResizable(el) {
            el.style.position = 'relative';

            var grip = createRowResizeElement();
            grip.addEventListener('mousedown', function (e) {
                rowElm = el;
                startOffset = el.offsetHeight - e.pageY;
            });

            el.appendChild(grip);
        }
    })();
</script>
</html>`
