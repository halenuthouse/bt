package html_templates

const Script = `    (function () {
        var colElm;
        var rowElm;
        var startOffset;

        Array.prototype.forEach.call(
            document.querySelectorAll("table td"),
            function (td) {
                makeColResizable(td);
                makeRowResizable(td);
            });

        Array.prototype.forEach.call(
            document.querySelectorAll("table th"),
            function (th) {
                makeColResizable(th);
            });

        document.addEventListener('mousemove', function (e) {
            if (colElm) {
                colElm.style.width = startOffset + e.pageX + 'px';
            }
            if (rowElm) {
                rowElm.style.height = startOffset + e.pageY + 'px';
            }
            console.log(rowElm, colElm);
        });

        document.addEventListener('mouseup', function () {
            colElm = undefined;
            rowElm = undefined;
        });

        function createColResizeElement() {
            var grip = document.createElement('div');
            grip.innerHTML = "&nbsp;";
            grip.style.top = 0;
            grip.style.right = 0;
            grip.style.bottom = 0;
            grip.style.width = '3px';
            grip.style.position = 'absolute';
            grip.style.cursor = 'col-resize';
            return grip;
        }

        function makeColResizable(el) {
            el.style.position = 'relative';

            var grip = createColResizeElement();
            grip.addEventListener('mousedown', function (e) {
                colElm = el;
                startOffset = el.offsetWidth - e.pageX;
            });

            el.appendChild(grip);
        }

        function createRowResizeElement() {
            var grip = document.createElement('div');
            grip.style.bottom = 0;
            grip.style.width = '100%';
            grip.style.height = '2px';
            grip.style.position = 'absolute';
            grip.style.cursor = 'row-resize';
			grip.style.display = 'block';
            return grip;
        }

        function makeRowResizable(el) {
            el.style.position = 'relative';

            var grip = createRowResizeElement();
            grip.addEventListener('mousedown', function (e) {
                rowElm = el;
                startOffset = el.offsetHeight - e.pageY;
            });

            el.appendChild(grip);
        }
    })();
`
