// +build windows

package display

import (
	"os"
	"golang.org/x/crypto/ssh/terminal"
)

func getConsoleWidth() int {
	handle := int(os.Stdout.Fd())
	width, _, err := terminal.GetSize(handle)
	if err != nil {
		return defaultConsoleWidth
	}

	return width
}

