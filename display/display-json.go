package display

import (
	"bt/statement"
	"cloud.google.com/go/bigtable"
	"encoding/json"
	"fmt"
	"log"
)

func (info *Info) initializeJSON(_ string) {
	info.PrintRow = info.printRowJSON
	info.Cleanup = info.cleanupJSON
}

func (info *Info) printRowJSON(row bigtable.Row) bool {
	if !info.headerPrinted {
		fmt.Fprintln(info.out, "[")
		info.headerPrinted = true
	} else {
		fmt.Fprintln(info.out, ",")
	}

	info.clearColumnStruct()
	key, record := info.rowToRecord(row)
	info.prepareForDisplay()

	// Start building JSON out map
	out := map[string]interface{}{}
	out["key"] = key

	for _, f := range info.displayFamilies {
		for _, q := range f.qualifiers {
			if q.option.Format == statement.Hide {
				continue
			}

			value, err := formatColumnForDisplay(f.family, q.qualifier, record, q.option)
			if err != nil {
				child := map[string]interface{}{q.qualifier: err.Error()}
				out["ERROR"] = child
			}
			if fjson, ok := out[f.family]; ok {
				fjson.(map[string]interface{})[q.qualifier] = string(value)
				out[f.family] = fjson
			} else {
				child := map[string]interface{}{q.qualifier: string(value)}
				out[f.family] = child
			}
		}
	}

	results, err := json.Marshal(out)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Fprint(info.out, string(results))
	info.recordCount++
	return true
}

func (info *Info) cleanupJSON() string {
	fmt.Fprintln(info.out, "\n]")
	return ""
}
