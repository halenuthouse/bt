package display

import (
	"time"

	"bt/statement"
	"cloud.google.com/go/bigtable"
)

func (info *Info) clearColumnStruct() {
	info.fieldMapping = []queryDisplayMapping{}
}

func (info *Info) rowToRecord(row bigtable.Row) (string, Record) {
	record := Record{}

	for fam := range row {
		for _, ri := range row[fam] {
			colName := statement.ParseColumn(ri.Column).Column
			info.addColumnToStruct(colName, len(ri.Value))

			ts := time.Unix(0, int64(ri.Timestamp)*1e3)
			record[ri.Column] = Cell{ri.Value, ts.Format(versionFormat)}
		}
	}

	return row.Key(), record
}

func (info *Info) addColumnToStruct(colName statement.Column, valueWidth int) {
	// If we don't have display fields, then initialize it from the query fields
	if len(info.fieldMapping) == 0 {
		for i := range info.queryFields {
			info.fieldMapping = append(info.fieldMapping, queryDisplayMapping{field: &info.queryFields[i]})
		}
	}

	for i := range info.fieldMapping {
		if info.fieldMapping[i].field.All ||
			(info.fieldMapping[i].field.Column == colName) ||
			(info.fieldMapping[i].field.Family == "" && info.fieldMapping[i].field.Qualifier == colName.Qualifier) ||
			(info.fieldMapping[i].field.Qualifier == "" && info.fieldMapping[i].field.Family == colName.Family) {
			dispIndex := 0
			for ; dispIndex<len(info.fieldMapping[i].display); dispIndex++ {
				if info.fieldMapping[i].display[dispIndex].column == colName {
					break
				}
			}

			if dispIndex == len(info.fieldMapping[i].display) {
				option := info.fieldMapping[i].field.Options
				if option.IsEmpty() {
					option = info.findColumnOption(colName)
					if option == nil {
						option = &statement.ColumnOption{}
					}
				}

				info.fieldMapping[i].display = append(info.fieldMapping[i].display,
					mapDispMap {
						column: colName,
						option: *option,
						width:  0,
					})
			}
			info.fieldMapping[i].display[dispIndex].width = max(info.fieldMapping[i].display[dispIndex].width, valueWidth)
		}
	}
}
