package config

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"os/user"
)

// ConfigFileName as name implies, defines the name of the config file. It must exist in the user's home directory
const ConfigFileName = "bt.json"

type (
	// Environment defines the bigtable connection information
	Environment struct {
		Name          string `json:"name"`
		Default       bool   `json:"default"`
		Project       string `json:"project"`
		Instance      string `json:"instance"`
		Creds         string `json:"creds"`
		AdminEndpoint string `json:"admin-endpoint"`
		DataEndpoint  string `json:"data-endpoint"`
	}

	// Table defines the table structure
	Table struct {
		Name string `json:"name"`
		Keys []Key  `json:"keys"`
	}

	// Key defines how primary key is defined
	Key struct {
		Pattern string `json:"pattern"`
		Type    string `json:"type"`
	}

	// Defaults defines settings that are used if not overridden by the command line arguments
	Defaults struct {
		ReadCount int `json:"readCount"`
	}

	// Config represents the configurat
	Config struct {
		Environments []Environment `json:"environments"`
		Tables       []Table       `json:"tables"`
		Defaults     Defaults      `json:"defaults"`
	}
)

// Read will retrieve the configuration from the file.
func Read() (Config, error) {
	cfg := Config{}
	fileName, err := FileName()
	if err != nil {
		return cfg, err
	}

	raw, err := ioutil.ReadFile(fileName)
	if err != nil {
		return cfg, err
	}

	err = json.Unmarshal(raw, &cfg)
	if err != nil {
		jerr := err.(*json.SyntaxError)
		begin := jerr.Offset - 20
		if begin < 0 {
			begin = 0
		}

		fmt.Println(string(raw[begin : begin+100]))
		return cfg, err
	}

	return cfg, nil
}

// FileName build the entire config file path
func FileName() (string, error) {
	usr, err := user.Current()
	if err != nil {
		return "", err
	}
	return usr.HomeDir + string(os.PathSeparator) + ConfigFileName, nil
}

// FindEnvironment determines the environment settings to be used
func (cfg *Config) FindEnvironment(name string) *Environment {
	for _, env := range cfg.Environments {
		if (name == "" && env.Default) || name == env.Name {
			return &env
		}
	}
	return nil
}

// FindTable looks up the table settings
func (cfg *Config) FindTable(name string) *Table {
	for _, tbl := range cfg.Tables {
		if name == tbl.Name {
			return &tbl
		}
	}
	return nil
}
