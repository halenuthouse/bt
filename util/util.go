package util

// FirstString returns the first string that has a value. If no passed in string has a value, then return the value of def.
func FirstString(def string, strs ...string) string {
	for _, str := range strs {
		if str != "" {
			return str
		}
	}

	return def
}

