package parser

import (
	"bt/flag"
	"bt/statement"
)

func (info *parseInfo) parseRead(flags *flag.Flags) (statement.Statement, error) {

	req := statement.Statement{Method: "READ"}
	value, isKey, isEnd, err := info.next(flags)
	if err != nil {
		return req, err
	}

	if isEnd {
		flags.AddFlag("help=select")
		return req, nil
	}

	if isKey && value == COLUMNS {

	}

	return req, nil
}
