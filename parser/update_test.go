package parser

import (
	"testing"

	"bt/statement"
)

// Test_update tests the parsing of the update statement
func Test_update(t *testing.T) {
	test(t, []TestDefinition{
		// Very simple UPDATE with a single = operator filter and two sets
		{name: "UPDATE single filter, 2 sets",
			sql: "UPDATE myTable SET fam:qual1=something, fam:qual2 = 2 WHERE id = 1",
			expected: statement.Statement{
				Method:    UPDATE,
				TableName: "myTable",
				Sets: []statement.SetField{
					{
						Column: statement.Column{"fam","qual1"},
						Value: []byte("something"),
					},
					{
						Column: statement.Column{"fam","qual2"},
						Value: []byte("2"),
					},
				},
				Filters: []statement.QueryFilter{
					{
						Field:  "id",
						Opr:    "=",
						Values: []string{"1"},
					},
				},
			}},

	})
}
