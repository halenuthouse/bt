package parser

import (
	"bt/flag"
	"bt/statement"
)

func (info *parseInfo) parseTable(flags *flag.Flags) (statement.Statement, error) {

	stmt := statement.Statement{Method: "TABLE"}
	str, _, isEnd, err := info.next(flags)
	if !isEnd {
		stmt.SubFunction = str

		str, _, isEnd, err = info.next(flags)
		if !isEnd {
			stmt.TableName = str
		}
	}

	return stmt, err
}
