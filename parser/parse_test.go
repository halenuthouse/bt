package parser

import (
	"fmt"
	"strconv"
	"strings"
	"testing"

	"bt/statement"
)

const failFormat = `Failed: %s was "%s", expected: "%s"`
const failSubFormat = `Failed: %s %s %s was "%s", expected: "%s"`
const failNotFoundFormat = `Failed: %s %s not found`

type (
	TestDefinition struct {
		name     string
		sql      string
		expected statement.Statement
	}
)

// test parsing
func test(t *testing.T, tests []TestDefinition) {
	for _, test := range tests {
		t.Log("Testing:", test.name)
		args := convertToCommandLineArgs(test.sql)
		actual, _, err := Parse(args)
		if err != nil {
			t.Error(err)
		}

		compare(t, actual, test.expected)
	}
}

func compare(t *testing.T, actual statement.Statement, expected statement.Statement) {
	if strings.ToUpper(actual.Method) != strings.ToUpper(expected.Method) {
		t.Errorf(failFormat, "Method Name", actual.Method, expected.Method)
	}

	if actual.SubFunction != expected.SubFunction {
		t.Errorf(failFormat, "SubFunction", actual.SubFunction, expected.SubFunction)
	}

	if actual.TableName != expected.TableName {
		t.Errorf(failFormat, "Table Name", actual.TableName, expected.TableName)
	}

	if actual.Limit != expected.Limit {
		t.Errorf(failFormat, "Limit", strconv.FormatInt(actual.Limit, 10), strconv.FormatInt(expected.Limit, 10))
	}

	compareFields(t, actual.Fields, expected.Fields)
	compareFilters(t, actual.Filters, expected.Filters)
	compareSets(t, actual.Sets, expected.Sets)
}

func compareSets(t *testing.T, actual []statement.SetField, expected []statement.SetField) {
	const comparetype = "Set"
	if len(actual) != len(expected) {
		t.Errorf("Failed: %s count was %d, expected %d", comparetype, len(actual), len(expected))
	} else {
		for i := range expected {
			if expected[i].Qualifier == actual[i].Qualifier && expected[i].Family == actual[i].Family {
				if len(expected[i].Value) != len(actual[i].Value) {
					t.Errorf("Failed: %s %s:%s length was %d, expected %d",
						comparetype,
						expected[i].Family,
						expected[i].Qualifier,
						len(actual[i].Value),
						len(expected[i].Value),
					)
				} else {
					same := true
					for pos, ch := range expected[i].Value {
						if actual[i].Value[pos] != ch {
							same = false
							break
						}
					}

					if !same {
						t.Errorf("Failed: %s %s:%s expected and actual values are not the same",
							comparetype,
							expected[i].Family,
							expected[i].Qualifier,
						)
					}
				}
			} else {
				t.Errorf("Failed: %s %s:%s actual field, but %s:%s was expected",
					comparetype,
					actual[i].Family,
					actual[i].Qualifier,
					expected[i].Family,
					expected[i].Qualifier,
				)
			}
		}
	}
}

func compareFilters(t *testing.T, actual []statement.QueryFilter, expected []statement.QueryFilter) {
	const comparetype = "Filter"

	if len(actual) != len(expected) {
		t.Errorf("Failed: %s count was %d, expected %d", comparetype, len(actual), len(expected))
	}

	for _, ef := range expected {
		found := false
		for _, af := range actual {
			if ef.Field == af.Field {
				found = true
				if ef.Opr != af.Opr {
					t.Errorf(failSubFormat, comparetype, ef.Field, "Opr", af.Opr, ef.Opr)
				}

				if len(ef.Values) != len(af.Values) {
					t.Errorf(`Failed: %s "%s" has %d values, but expected %d`, comparetype, ef.Field, len(af.Values), len(ef.Values))
				} else {
					for i := range ef.Values {
						if ef.Values[i] != af.Values[i] {
							t.Errorf(`Failed: %s "%s" Value was, "%s" expected, "%s"`, comparetype, fmt.Sprintf("%s[%d]", ef.Field, i), af.Values[i], ef.Values[i])
						}
					}
				}
				break
			}
		}
		if !found {
			t.Errorf(failNotFoundFormat, comparetype, ef.Field)
		}
	}

	for _, af := range actual {
		found := false
		for _, ef := range expected {
			if ef.Field == af.Field {
				found = true
				break
			}
		}
		if !found {
			t.Errorf(failNotFoundFormat, comparetype, af.Field)
		}
	}
}

func compareFields(t *testing.T, actual []statement.Field, expected []statement.Field) {
	const comparetype = "Field"

	if len(actual) != len(expected) {
		t.Errorf("Failed: %s count was %d, expected %d", comparetype, len(actual), len(expected))
	} else {
		for i := range expected {
			if expected[i].Qualifier == actual[i].Qualifier && expected[i].Family == actual[i].Family {
				if expected[i].All != actual[i].All {
					t.Errorf(failFormat, fmt.Sprintf("%s[%d].All", comparetype, i), strconv.FormatBool(actual[i].All), strconv.FormatBool(expected[i].All))
				}

				compareOption(t, expected[i].Column, expected[i].Options, actual[i].Options)

				//if len(ef.BTFields) != len(af.BTFields) {
				//	t.Errorf(`Failed: %s has %d columns, but expected %d`, comparetype, len(af.BTFields), len(ef.BTFields))
				//} else {
				//	for i := range ef.BTFields {
				//		if ef.BTFields[i] != af.BTFields[i] {
				//			t.Errorf(`Failed: %s "%s" big table field was, "%s" expected, "%s"`, comparetype, ef.Qualifier+":"+ef.Family, af.BTFields[i], ef.BTFields[i])
				//		}
				//	}
				//}
			} else {
				t.Errorf(failFormat, fmt.Sprintf("%s[%d]", comparetype, i), actual[i].Qualifier+":"+actual[i].Family, expected[i].Qualifier+":"+expected[i].Family)
			}
		}
	}
}

func compareOption(t *testing.T, column statement.Column, expected *statement.ColumnOption, actual *statement.ColumnOption) {
	if actual == nil && expected == nil {
		return
	}
	if actual == nil {
		actual = &statement.ColumnOption{}
	}
	if expected == nil {
		expected = &statement.ColumnOption{}
	}
	if expected.Format != actual.Format {
		t.Errorf(failSubFormat, "Fields", column.Name(), "Options.Format", actual.Format, expected.Format)
	}
	if expected.ResizeStrategy != actual.ResizeStrategy {
		t.Errorf(failSubFormat, "Fields", column.Name(), "Options.ResizeStrategy", actual.ResizeStrategy, expected.ResizeStrategy)
	}
	if expected.Size != actual.Size {
		t.Errorf(failSubFormat, "Fields", column.Name(), "Options.Size", strconv.Itoa(actual.Size), strconv.Itoa(expected.Size))
	}
}

func convertToCommandLineArgs(sql string) []string {
	ret := []string{""}
	current := ""
	inQuote := false

	for _, ch := range sql {
		if ch == ' ' && !inQuote {
			ret = append(ret, current)
			current = ""
			continue
		}
		if ch == '"' {
			if inQuote {
				inQuote = false
			} else {
				inQuote = true
			}
			continue
		}
		current += string(ch)
	}

	if current != "" {
		ret = append(ret, current)
	}

	return ret
}
