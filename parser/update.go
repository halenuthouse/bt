package parser

import (
	"fmt"

	"bt/flag"
	"bt/statement"
)

func (info *parseInfo) parseUpdate(flags *flag.Flags) (statement.Statement, error) {

	req := statement.Statement{Method: "UPDATE"}

	var value string
	var isKey, isEnd bool
	var err error

	// table name
	value, isKey, isEnd, err = info.next(flags)
	if err != nil {
		return req, err
	}

	if isEnd {
		flags.AddFlag("help=update")
		return req, nil
	}

	req.TableName = value

	// sets
	value, isKey, isEnd, err = info.next(flags)
	if err != nil {
		return req, err
	}

	if !isKey || value != SET {
		return req, fmt.Errorf(`Expected "SET", found "%s"`, value)
	}

	req.Sets, value, isKey, isEnd, err = info.parseSetFields(flags)
	if err != nil {
		return req, err
	}

	if isEnd {
		return req, fmt.Errorf("Unexpected END")
	}

	if value == "WHERE" {
		filter, err := info.parseFilter(flags)
		if err != nil {
			return req, err
		}
		req.Filters = filter
	}

	value, isKey, isEnd, err = info.next(flags)
	if err != nil {
		return req, err
	}

	if !isEnd {
		return req, fmt.Errorf(`Expected END, but found "%s"`, value)
	}

	return req, nil
}

