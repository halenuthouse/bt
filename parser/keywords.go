package parser

// Constants for the keywords
const (
	COLUMNS   = "COLUMNS"
	CREATE    = "CREATE"
	DELETE    = "DELETE"
	DELETEROW = "DELETEROW"
	DROP      = "DROP"
	LIST      = "LIST"
	READ      = "READ"
	SELECT    = "SELECT"
	INSERT    = "INSERT"
	TABLE     = "TABLE"
	UPDATE    = "UPDATE"
	UPDATEROW = "UPDATEROW"
	FROM      = "FROM"
	WHERE     = "WHERE"
	AND       = "AND"
	LIMIT     = "LIMIT"
	CONTAINS  = "CONTAINS"
	IN        = "IN"
	SET       = "SET"
)

var keywords = []string{
	COLUMNS,
	CREATE,
	DELETE,
	DELETEROW,
	DROP,
	LIST,
	READ,
	SELECT,
	TABLE,
	UPDATE,
	UPDATEROW,
	FROM,
	WHERE,
	AND,
	LIMIT,
	CONTAINS,
	SET,
	INSERT,
}
