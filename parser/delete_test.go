package parser

import (
	"testing"

	"bt/statement"
)

// Test_delete tests the parsing of the delete statement
func Test_delete(t *testing.T) {
	test(t, []TestDefinition{
		// Very simple DELETE with a single = operator filter
		{name: "DELETE single = filter",
			sql: "DELETE FROM myTable WHERE id = 1",
			expected: statement.Statement{
				Method:    DELETE,
				TableName: "myTable",
				Filters: []statement.QueryFilter{
					{
						Field:  "id",
						Opr:    "=",
						Values: []string{"1"},
					},
				},
			}},

		// Test multiple simple = operator filters
		{name: "DELETE multiple filters",
			sql: `DELETE FROM myTable WHERE id = 1 AND something = "else"`,
			expected: statement.Statement{
				Method:    DELETE,
				TableName: "myTable",
				Filters: []statement.QueryFilter{
					{
						Field:  "id",
						Opr:    "=",
						Values: []string{"1"},
					},
					{
						Field:  "something",
						Opr:    "=",
						Values: []string{"else"},
					},
				},
			}},

		// Test single IN clause combined with simple = operator filters
		{name: "DELETE multiple filters with IN clause",
			sql: `DELETE FROM myTable WHERE id = 1 AND something = "else" AND OneOf in(10, "another value", "102934-9382902")`,
			expected: statement.Statement{
				Method:    DELETE,
				TableName: "myTable",
				Filters: []statement.QueryFilter{
					{
						Field:  "id",
						Opr:    "=",
						Values: []string{"1"},
					},
					{
						Field:  "something",
						Opr:    "=",
						Values: []string{"else"},
					},
					{
						Field:  "OneOf",
						Opr:    "IN",
						Values: []string{"10", "another value", "102934-9382902"},
					},
				},
			}},

		// Test multiple IN clause combined with simple = operator filters
		{name: "DELETE multiple filters with multiple IN clause",
			sql: `DELETE FROM myTable WHERE id = 1 AND something = "else" AND OneOf in(10, "another value", "102934-9382902") AND Other in( 5, 6, 7, 8 )`,
			expected: statement.Statement{
				Method:    DELETE,
				TableName: "myTable",
				Filters: []statement.QueryFilter{
					{
						Field:  "id",
						Opr:    "=",
						Values: []string{"1"},
					},
					{
						Field:  "something",
						Opr:    "=",
						Values: []string{"else"},
					},
					{
						Field:  "OneOf",
						Opr:    "IN",
						Values: []string{"10", "another value", "102934-9382902"},
					},
					{
						Field:  "Other",
						Opr:    "IN",
						Values: []string{"5", "6", "7", "8"},
					},
				},
			}},
	})
}
