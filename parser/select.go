package parser

import (
	"fmt"
	"strconv"

	"bt/flag"
	"bt/statement"
)

func (info *parseInfo) parseSelect(flags *flag.Flags) (statement.Statement, error) {

	req := statement.Statement{Method: READ, Raw: info.raw()}
	var value string
	var isKey, isEnd bool
	var err error

	req.Fields, value, isKey, isEnd, err = info.parseFields(flags)
	if err != nil {
		return req, err
	}

	if isEnd {
		flags.AddFlag("help=select")
		return req, nil
	}

	if len(req.Fields) == 0 {
		req.Fields = append(req.Fields, statement.Field{All: true})
	}

	if !isKey || value != FROM {
		return req, fmt.Errorf(`Expected "FROM", found "%s"`, value)
	}

	value, isKey, isEnd, err = info.next(flags)
	if err != nil {
		return req, err
	}
	if isEnd {
		return req, fmt.Errorf("Unexpected END")
	}

	req.TableName = value

	for {
		value, isKey, isEnd, err = info.next(flags)
		if err != nil {
			return req, err
		}

		if isEnd {
			break
		}

		if isKey {
			switch value {
			case WHERE:
				filter, err := info.parseFilter(flags)
				if err != nil {
					return req, err
				}
				req.Filters = filter
				continue
			case LIMIT:
				value, isKey, isEnd, err = info.next(flags)
				if err != nil {
					return req, err
				}

				if !isEnd {
					num, err := strconv.ParseInt(value, 10, 0)
					if err != nil {
						return req, err
					}
					req.Limit = num
				}
				continue
			}
		}
		return req, fmt.Errorf(`Expected "WHERE" or "LIMIT", found "%s"`, value)
	}

	return req, nil
}
