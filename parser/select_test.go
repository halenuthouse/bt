package parser

import (
	"testing"

	"bt/statement"
)

func Test_select(t *testing.T) {
	test(t, []TestDefinition{
		// Very simple SELECT with all fields and a single = operator filter
		{name: "SELECT whitespace testing",
			sql: "SELECT * FROM myTable WHERE id1 = 1 and id2=2 and id3= 3 and id4 =4",
			expected: statement.Statement{
				Method: READ,
				Fields: []statement.Field{
					{All: true},
				},
				TableName: "myTable",
				Filters: []statement.QueryFilter{
					{
						Field:  "id1",
						Opr:    "=",
						Values: []string{"1"},
					},
					{
						Field:  "id2",
						Opr:    "=",
						Values: []string{"2"},
					},
					{
						Field:  "id3",
						Opr:    "=",
						Values: []string{"3"},
					},
					{
						Field:  "id4",
						Opr:    "=",
						Values: []string{"4"},
					},
				},
			}},

		// Very simple SELECT with all fields and a single = operator filter
		{name: "SELECT all fields with single = filter",
			sql: "SELECT * FROM myTable WHERE id = 1",
			expected: statement.Statement{
				Method: READ,
				Fields: []statement.Field{
					{All: true},
				},
				TableName: "myTable",
				Filters: []statement.QueryFilter{
					{
						Field:  "id",
						Opr:    "=",
						Values: []string{"1"},
					},
				},
			}},

		// Very simple SELECT with all fields as a default and a single = operator filter
		{name: "SELECT all as default with single = filter",
			sql: "SELECT FROM myTable WHERE id = 1",
			expected: statement.Statement{
				Method: READ,
				Fields: []statement.Field{
					{All: true},
				},
				TableName: "myTable",
				Filters: []statement.QueryFilter{
					{
						Field:  "id",
						Opr:    "=",
						Values: []string{"1"},
					},
				},
			}},

		// Very simple SELECT with all fields as a default and a single = operator filter
		{name: "SELECT all as default with single=filter",
			sql: "SELECT FROM myTable WHERE id=1",
			expected: statement.Statement{
				Method: READ,
				Fields: []statement.Field{
					{All: true},
				},
				TableName: "myTable",
				Filters: []statement.QueryFilter{
					{
						Field:  "id",
						Opr:    "=",
						Values: []string{"1"},
					},
				},
			}},

		// Test specific columns with simple = operator filters
		{name: "SELECT specific columns with simple = filter",
			sql: `SELECT FamOnly:, :QualOnly, Fam:Qual FROM myTable WHERE id = 1`,
			expected: statement.Statement{
				Method: READ,
				Fields: []statement.Field{
					{Column: statement.Column{Family: "FamOnly"}},
					{Column: statement.Column{Qualifier: "QualOnly"}},
					{Column: statement.Column{"Fam","Qual"}},
				},
				TableName: "myTable",
				Filters: []statement.QueryFilter{
					{
						Field:  "id",
						Opr:    "=",
						Values: []string{"1"},
					},
				},
			}},

		// Test multiple simple = operator filters
		{name: "SELECT multiple filters",
			sql: `SELECT FROM myTable WHERE id = 1 AND something = "else"`,
			expected: statement.Statement{
				Method: READ,
				Fields: []statement.Field{
					{All: true},
				},
				TableName: "myTable",
				Filters: []statement.QueryFilter{
					{
						Field:  "id",
						Opr:    "=",
						Values: []string{"1"},
					},
					{
						Field:  "something",
						Opr:    "=",
						Values: []string{"else"},
					},
				},
			}},

		// Test single IN clause combined with simple = operator filters
		{name: "SELECT multiple filters with IN clause",
			sql: `SELECT FROM myTable WHERE id = 1 AND something = "else" AND OneOf in 10, "another value", "102934-9382902"`,
			expected: statement.Statement{
				Method: READ,
				Fields: []statement.Field{
					{All: true},
				},
				TableName: "myTable",
				Filters: []statement.QueryFilter{
					{
						Field:  "id",
						Opr:    "=",
						Values: []string{"1"},
					},
					{
						Field:  "something",
						Opr:    "=",
						Values: []string{"else"},
					},
					{
						Field:  "OneOf",
						Opr:    "IN",
						Values: []string{"10", "another value", "102934-9382902"},
					},
				},
			}},

		// Test multiple IN clause combined with simple = operator filters
		{name: "SELECT multiple filters with multiple IN clause",
			sql: `SELECT FROM myTable WHERE id = 1 AND something = "else" AND OneOf in 10, "another value", "102934-9382902" AND Other in 5, 6, 7, 8`,
			expected: statement.Statement{
				Method: READ,
				Fields: []statement.Field{
					{All: true},
				},
				TableName: "myTable",
				Filters: []statement.QueryFilter{
					{
						Field:  "id",
						Opr:    "=",
						Values: []string{"1"},
					},
					{
						Field:  "something",
						Opr:    "=",
						Values: []string{"else"},
					},
					{
						Field:  "OneOf",
						Opr:    "IN",
						Values: []string{"10", "another value", "102934-9382902"},
					},
					{
						Field:  "Other",
						Opr:    "IN",
						Values: []string{"5", "6", "7", "8"},
					},
				},
			}},

		// Test multiple IN clause combined with simple = operator filters
		{name: "SELECT multiple filters (no whitespace) with multiple IN clause",
			sql: `SELECT FROM myTable WHERE id=1 AND something="else" AND OneOf in 10, "another value", "102934-9382902" AND Other in 5, 6, 7, 8`,
			expected: statement.Statement{
				Method: READ,
				Fields: []statement.Field{
					{All: true},
				},
				TableName: "myTable",
				Filters: []statement.QueryFilter{
					{
						Field:  "id",
						Opr:    "=",
						Values: []string{"1"},
					},
					{
						Field:  "something",
						Opr:    "=",
						Values: []string{"else"},
					},
					{
						Field:  "OneOf",
						Opr:    "IN",
						Values: []string{"10", "another value", "102934-9382902"},
					},
					{
						Field:  "Other",
						Opr:    "IN",
						Values: []string{"5", "6", "7", "8"},
					},
				},
			}},

		// Test multiple IN clause combined with simple = operator filters (with parentheses)
		{name: "SELECT multiple filters with multiple IN clause (with parentheses)",
			sql: `SELECT FROM myTable WHERE id = 1 AND something = "else" AND OneOf in( 10, "another value", "102934-9382902") AND Other in(5, 6, 7, 8)`,
			expected: statement.Statement{
				Method: READ,
				Fields: []statement.Field{
					{All: true},
				},
				TableName: "myTable",
				Filters: []statement.QueryFilter{
					{
						Field:  "id",
						Opr:    "=",
						Values: []string{"1"},
					},
					{
						Field:  "something",
						Opr:    "=",
						Values: []string{"else"},
					},
					{
						Field:  "OneOf",
						Opr:    "IN",
						Values: []string{"10", "another value", "102934-9382902"},
					},
					{
						Field:  "Other",
						Opr:    "IN",
						Values: []string{"5", "6", "7", "8"},
					},
				},
			}},

		// Test multiple IN clause combined with simple = operator filters (with parentheses)
		{name: "SELECT simple filter with LIMIT",
			sql: `SELECT FROM myTable WHERE name = steve limit 1000`,
			expected: statement.Statement{
				Method: READ,
				Fields: []statement.Field{
					{All: true},
				},
				TableName: "myTable",
				Filters: []statement.QueryFilter{
					{
						Field:  "name",
						Opr:    "=",
						Values: []string{"steve"},
					},
				},
				Limit: 1000,
			}},

		// Test multiple IN clause combined with simple = operator filters (with parentheses)
		{name: "SELECT multiple filters with LIMIT",
			sql: `SELECT FROM myTable WHERE id = 1 AND something = "else" AND OneOf in( 10, "another value", "102934-9382902") AND Other in(5, 6, 7, 8) limit 1000`,
			expected: statement.Statement{
				Method: READ,
				Fields: []statement.Field{
					{All: true},
				},
				TableName: "myTable",
				Filters: []statement.QueryFilter{
					{
						Field:  "id",
						Opr:    "=",
						Values: []string{"1"},
					},
					{
						Field:  "something",
						Opr:    "=",
						Values: []string{"else"},
					},
					{
						Field:  "OneOf",
						Opr:    "IN",
						Values: []string{"10", "another value", "102934-9382902"},
					},
					{
						Field:  "Other",
						Opr:    "IN",
						Values: []string{"5", "6", "7", "8"},
					},
				},
				Limit: 1000,
			}},

		// Test specific columns with simple = operator filters
		{name: "SELECT options",
			sql: `SELECT FamOnly:[hide], :QualOnly[json|100], Fam:Qual[100], Fam:Qual1[hex] FROM myTable WHERE id=1`,
			expected: statement.Statement{
				Method: READ,
				Fields: []statement.Field{
					{
						Column: statement.Column{Family: "FamOnly"},
						Options: &statement.ColumnOption{
							Format:"hide",
						},
					},
					{
						Column: statement.Column{Qualifier: "QualOnly"},
						Options: &statement.ColumnOption{
							Format:"JSON",
							Size:100,
						},
					},
					{
						Column: statement.Column{"Fam","Qual"},
						Options: &statement.ColumnOption{
							Size:100,
						},
					},
					{
						Column: statement.Column{"Fam","Qual1"},
						Options: &statement.ColumnOption{
							Format: "HEX",
						},
					},
				},
				TableName: "myTable",
				Filters: []statement.QueryFilter{
					{
						Field:  "id",
						Opr:    "=",
						Values: []string{"1"},
					},
				},
			}},

	})
}
