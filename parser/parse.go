package parser

import (
	"fmt"
	"regexp"

	"strings"

	"bt/flag"
	"bt/statement"
)

type parseInfo struct {
	args  []string
	index int
}

// Parse the command line into flags and the request
func Parse(args []string) (stmt statement.Statement, flags flag.Flags, err error) {

	info := parseInfo{
		args:  args,
		index: 0,
	}

	value, isKey, isEnd, err := info.next(&flags)
	if err != nil {
		return statement.Statement{}, flags, err
	}

	if isKey {
		switch value {
		case SELECT:
			stmt, err = info.parseSelect(&flags)
			return
		case READ:
			stmt, err = info.parseRead(&flags)
			return
		case TABLE:
			stmt, err = info.parseTable(&flags)
			return
		case DELETE:
			stmt, err = info.parseDelete(&flags)
			return
		case UPDATE:
			stmt, err = info.parseUpdate(&flags)
			return
		}
	}

	if err != nil {
		return
	}

	if isEnd {
		flags.AddFlag("help")
		return statement.Statement{}, flags, nil
	}

	return statement.Statement{}, flags, fmt.Errorf(`For help use "-help"`)
}

func (info *parseInfo) raw() string {
	ret := []string{}
	for _, arg := range info.args {
		if len(arg) > 1 && arg[0] != '-' {
			ret = append(ret, arg)
		}
	}

	return strings.Join(ret, " ")
}

func (info *parseInfo) prev() {
	info.index--
}

func (info *parseInfo) next(flags *flag.Flags) (value string, isKey bool, isEnd bool, err error) {
	err = info.parseFlag(flags)
	if err != nil {
		return
	}

	if info.index >= len(info.args) {
		isEnd = true
	} else {
		upper := strings.ToUpper(info.args[info.index])
		for _, kw := range keywords {
			if kw == upper {
				isKey = true
				value = upper
				break
			}
		}

		if !isKey {
			value = info.args[info.index]
		}
		info.index++
	}
	return
}

func (info *parseInfo) parseFields(flags *flag.Flags) (fields []statement.Field, value string, isKey bool, isEnd bool, err error) {
	for {
		value, isKey, isEnd, err = info.next(flags)
		if isKey || isEnd {
			break
		}
		if value == "," {
			continue
		}

		if value[len(value)-1] == ',' {
			value = value[:len(value)-1]
		}

		field := statement.Field{}
		if value == "*" {
			field.All = true
		} else {
			field = statement.ParseColumn(value)
		}

		fields = append(fields, field)
	}

	return
}

func (info *parseInfo) parseSetFields(flags *flag.Flags) (fields []statement.SetField, value string, isKey bool, isEnd bool, err error) {
	var re = regexp.MustCompile(`(?m)(\w*):(\w*)(.*)`)
	for {
		value, isKey, isEnd, err = info.next(flags)
		if isKey {
			break
		}
		if value == "," {
			continue
		}

		if value[len(value)-1] == ',' {
			value = value[:len(value)-1]
		}

		field := statement.SetField{}
		for _, match := range re.FindAllStringSubmatch(value, -1) {
			field.Family = match[1]
			field.Qualifier = match[2]

			if match[3] == "" {
				value, isKey, isEnd, err = info.next(flags)
				if isKey {
					break
				}

				if value[0] == '=' {
					if len(value) > 1 {
						field.Value = []byte(value[1:])
					} else {
						value, isKey, isEnd, err = info.next(flags)
						if isKey {
							break
						}
						field.Value = []byte(value)
					}
				} else {
					err = fmt.Errorf(`expected "=", but got "%s"`, value)
					return
				}
			} else {
				if match[3][0] != '=' {
					err = fmt.Errorf(`unexpected "%s"`, match[3])
					return
				}

				field.Value = []byte(match[3][1:])
			}
		}

		if field.Family == "" || field.Qualifier == "" {
			err = fmt.Errorf(`SET fields must have both Family and Qualifiers defined`)
			return
		}

		if len(field.Value) == 0 {
			err = fmt.Errorf(`SET %s:%s value is missing`, field.Family, field.Qualifier)
			return
		}

		fields = append(fields, field)
	}

	return
}

func (info *parseInfo) parseFilter(flags *flag.Flags) ([]statement.QueryFilter, error) {
	filters := []statement.QueryFilter{}
	filter := statement.QueryFilter{}
	for {
		value, isKey, isEnd, err := info.next(flags)
		if err != nil {
			return filters, err
		}
		if isKey && value == LIMIT {
			info.prev()
			isEnd = true
		}

		if isEnd {
			err = validateFilter(filter)
			if err != nil {
				return filters, err
			}
			filters = append(filters, filter)
			break
		}

		if isKey && value == AND {
			err = validateFilter(filter)
			if err != nil {
				return filters, err
			}
			filters = append(filters, filter)
			filter = statement.QueryFilter{}
			continue
		}

		if filter.Field == "" {
			split := strings.Split(value, "=")
			if len(split) == 1 {
				filter.Field = value
			} else {
				filter.Field = split[0]
				filter.Opr = "="
				if split[1] != "" {
					filter.Values = []string{value[strings.Index(value, "=")+1:]}
				}
			}
			continue
		}

		if isKey && value == CONTAINS {
			filter.Opr = value
			continue
		}

		if filter.Opr == "" {
			if strings.ToUpper(value) == "IN" || len(value) >= 3 && strings.ToUpper(value[:3]) == "IN(" {
				filter.Opr = IN
				filter.Values, err = parseInClause(info, flags, value)
				if err != nil {
					return filters, nil
				}
				continue
			}

			if value[0] == '=' {
				filter.Opr = "="
				if len(value) > 1 {
					filter.Values = []string{value[1:]}
				}
				continue
			}

			if value == CONTAINS {
				filter.Opr = CONTAINS
			} else {
				return filters, fmt.Errorf(`Expected "=", "IN" or "CONTAINS", found "%s"`, value)
			}
			continue
		}

		filter.Values = append(filter.Values, value)
	}

	return filters, nil
}

func parseInClause(info *parseInfo, flags *flag.Flags, value string) ([]string, error) {
	ret := []string{}
	if len(value) >= 3 && strings.ToUpper(value[:3]) == "IN(" {
		cleaned, isEndIn := cleanInValue(value[3:])
		if cleaned != "" {
			ret = append(ret, cleaned)
		}
		if isEndIn {
			return ret, nil
		}
	}

	for {
		value, _, _, err := info.next(flags)
		if err != nil {
			return ret, err
		}

		if value == ")" {
			break
		}

		cleaned, isEndIn := cleanInValue(value)
		if cleaned != "" {
			ret = append(ret, cleaned)
		}

		if isEndIn {
			break
		}
	}
	return ret, nil
}

func cleanInValue(value string) (string, bool) {
	isEndIn := false
	if value != "" {
		switch value[len(value)-1] {
		case ')':
			isEndIn = true
			fallthrough
		case ',':
			value = value[:len(value)-1]
		default:
			isEndIn = true

		}
	}

	return value, isEndIn
}

func (info *parseInfo) parseFlag(flags *flag.Flags) error {
	for info.index < len(info.args) && len(info.args[info.index]) > 1 && info.args[info.index][0] == '-' {
		added, err := flags.AddFlag(info.args[info.index][1:])
		if err != nil {
			return err
		}

		if !added {
			return fmt.Errorf(`Unknown flag "%s"`, info.args[info.index])
		}

		info.index++
	}

	return nil
}

func validateFilter(filter statement.QueryFilter) error {
	if filter.Field == "" {
		return fmt.Errorf("Field not specified in filter")
	}
	if filter.Opr == "" {
		return fmt.Errorf("Opr not specified in filter")
	}
	if len(filter.Values) == 0 {
		return fmt.Errorf("Value not specified in filter")
	}
	return nil
}
