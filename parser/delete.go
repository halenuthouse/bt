package parser

import (
	"fmt"

	"bt/flag"
	"bt/statement"
)

func (info *parseInfo) parseDelete(flags *flag.Flags) (statement.Statement, error) {

	req := statement.Statement{Method: "DELETE", Raw: info.raw()}
	var value string
	var isKey, isEnd bool
	var err error

	req.Fields, value, isKey, isEnd, err = info.parseFields(flags)
	if err != nil {
		return req, err
	}

	if isEnd {
		flags.AddFlag("help=delete")
		return req, nil
	}

	if !isKey || value != FROM {
		return req, fmt.Errorf(`Expected "FROM", found "%s"`, value)
	}

	value, isKey, isEnd, err = info.next(flags)
	if err != nil {
		return req, err
	}
	if isEnd {
		return req, fmt.Errorf("Unexpected END")
	}

	req.TableName = value

	for {
		value, isKey, isEnd, err = info.next(flags)
		if err != nil {
			return req, err
		}

		if isEnd {
			break
		}

		if isKey {
			if value == WHERE {
				filter, err := info.parseFilter(flags)
				if err != nil {
					return req, err
				}
				req.Filters = filter
				continue
			}
		}
		return req, fmt.Errorf(`Expected "WHERE", found "%s"`, value)
	}

	return req, nil
}
