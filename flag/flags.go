package flag

import (
	"fmt"
	"os"
	"regexp"
	"strings"
)

// Help Subjects
const (
	FlagFormat      = "fmt"
	FlagFormatHoriz = "horiz"
	FlagFormatVert  = "vert"
	FlagFormatJSON  = "json"
	FlagFormatHTML  = "html"

	FlagCode  = "code"

	FlagHelp       = "help"
	FlagHelpSelect = "select"
	FlagHelpRead   = "read"
	FlagHelpTable  = "table"

	FlagEnv = "env"
	FlagVersion = "version"
	FlagDisplayOption = "o"
	FlagRowStruct = "struct"

	FlagDebug = "debug"
	FlagServer = "server"
)

// Info defines the information about a flag
type Info struct {
	Flags       []string
	Values      []string
	DefValue    string
	DefNoValue  string
	Description string
}

// Flag is the actual flag
type Flag struct {
	Key   string
	Value string
}

// Flags is an array of Flag
type Flags []Flag

var flagInfos = map[string]Info{
	FlagFormat: {
		Flags:       []string{"fmt", "f"},
		Description: `Specifies the format of the read output. "h" horizontal (default), "v" vertical, "json", "code"`,
	},
	FlagFormatHoriz: {
		Flags:       []string{FlagFormatHoriz},
		DefValue: "1",
		DefNoValue: "1",
	},
	FlagFormatHTML: {
		Flags:       []string{FlagFormatHTML},
		DefValue: "0",
	},
	FlagFormatVert: {
		Flags:       []string{FlagFormatVert},
		DefValue: "0",
	},
	FlagFormatJSON: {
		Flags:       []string{FlagFormatJSON},
		DefValue: "0",
	},
	FlagHelp: {
		Flags: []string{"h", "help"},
		Values: []string{
			FlagHelpSelect,
			FlagHelpRead,
			FlagHelpTable,
		},
	},
	FlagEnv: {
		Flags: []string{"e"},
	},
	FlagVersion: {
		Flags:      []string{"v"},
		DefValue:   "0",
		DefNoValue: "1",
		Values: []string{
			"0", "1",
			"false", "true",
		},
	},
	FlagDisplayOption: {
		Flags: []string{"o"},
	},
	FlagRowStruct: {
		Flags:      []string{FlagRowStruct},
		DefValue:   "s",
		DefNoValue: "u",
	},
	FlagDebug: {
		Flags: []string{FlagDebug},
		DefValue:   "0",
		DefNoValue: "1",
	},
	FlagCode: {
		Flags: []string{FlagCode},
		DefValue:   "",
		DefNoValue: "go",
	},
	FlagServer: {
		Flags: []string{FlagServer, "s"},
	},
}

// AddFlag adds a single flag to the array of flags
func (flags *Flags) AddFlag(rawFlag string) (bool, error) {
	split := strings.Split(rawFlag, "=")
	for key, flag := range flagInfos {
		for _, f := range flag.Flags {
			if split[0] == f {
				var newFlag Flag
				if len(split) > 1 {
					newFlag = Flag{key, split[1]}
				} else {
					newFlag = Flag{key, ""}
				}

				if len(flag.Values) != 0 {
					if len(split) == 1 {
						newFlag.Value = flag.DefNoValue
					} else {
						found := false
						for _, value := range flag.Values {
							if value == split[1] {
								found = true
								break
							}
						}
						if !found {
							return false, fmt.Errorf(`flag "-%s" not valid`, rawFlag)
						}
					}
				}

				*flags = append(*flags, newFlag)
				return true, nil
			}
		}
	}
	return false, nil
}

// Help determines if any of the flags is a "call for help"
func (flags Flags) Help() (string, bool) {
	return flags.hasFlag(FlagHelp)
}

// FindArg directly looks at the OS args to see if we have a request for help
func FindArg(values ...string) (string, bool) {
	for _, arg := range os.Args[1:] {
		if len(arg) < 1 || arg[0] != '-' {
			continue
		}

		split := strings.Split(arg, "=")
		found := false
		for _, value := range values {
			if split[0] == "-" + value {
				found = true
			}
		}
		if found {
			if len(split) > 1 {
				return split[1], true
			}
			return "", true
		}
	}

	return "", false
}

// Environment checks for an override of the environment
func (flags Flags) Environment() string {
	env, _ := flags.hasFlag(FlagEnv)
	return env
}

// Format checks for an override of the output format
func (flags Flags) Format() (string, string) {
	for _, f := range []string{FlagFormatJSON, FlagFormatHTML, FlagFormatVert, FlagFormatHoriz} {
		if _, opt, has := flags.hasFlagWithOption(f); has {
			return f, opt
		}
	}

	f := flags.hasFlagDefault(FlagFormat)
	re := regexp.MustCompile(`(?m)(\w*)(.*)`)
	split := re.FindStringSubmatch(f)
	format := split[1]
	formatOption:= strings.NewReplacer("[","","]","", "(","",")","").Replace(split[2])

	return format, formatOption
}

// ShowColumnVersion checks for showing the version
func (flags Flags) ShowColumnVersion() bool {
	show := flags.hasFlagDefault(FlagVersion)
	return convertBool(show)
}

// Debug checks for showing debug information
func (flags Flags) Debug() bool {
	show := flags.hasFlagDefault(FlagDebug)
	return convertBool(show)
}

// Debug checks for showing debug information
func (flags Flags) Code() string {
	lang := flags.hasFlagDefault(FlagCode)
	return lang
}

// ColumnOptions returns any column options specified un-parsed
func (flags Flags) ColumnOptions() []string {
	ret := []string{}
	for _, flag := range flags {
		if flag.Key == "o" {
			ret = append(ret, flag.Value)
		}
	}
	return ret
}

// ShowColumnVersion checks for showing the version
func (flags Flags) SameRowStruct() bool {
	value := flags.hasFlagDefault(FlagRowStruct)
	return strings.ToLower(value) == "s"
}

func convertBool(value string) bool {
	return value == "1" || value == "true"
}

func (flags Flags) hasFlagWithOption(flagKey string) (string, string, bool) {
	re := regexp.MustCompile(`(?m)(\w*)(.*)`)
	for _, flag := range flags {
		split := re.FindStringSubmatch(flag.Key)
		if split[1] == flagKey {
			return flag.Value, strings.NewReplacer("[","","]","", "(","",")","").Replace(split[2]), true
		}
	}

	return "", "", false
}

func (flags Flags) hasFlag(flagKey string) (string, bool) {
	for _, flag := range flags {
		if flag.Key == flagKey {
			return flag.Value, true
		}
	}
	return "", false
}

func (flags Flags) hasFlagDefault(flagKey string) string {
	value, found := flags.hasFlag(flagKey)
	if found {
		if value == "" {
			return flagInfos[flagKey].DefNoValue
		}
		return value
	}
	return flagInfos[flagKey].DefValue
}
