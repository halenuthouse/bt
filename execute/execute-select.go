package execute

import (

"bt/display"
"bt/flag"
"bt/statement"
"context"
	"os"

	"cloud.google.com/go/bigtable"


)

func selectInitialize(_ context.Context, stmt *statement.Statement, flags flag.Flags, out *os.File) (interface{}, error) {
	return display.New(stmt, flags, out), nil
}

func selectProcess(_ context.Context, _ *statement.Statement, _ *bigtable.Table, row *bigtable.Row, data interface{}) (bool, error) {

	info := data.(*display.Info)
	info.PrintRow(*row)

	return true, nil
}

func selectCleanup(data interface{}) (string, error) {
	info := data.(*display.Info)
	return info.Cleanup(), nil
}
