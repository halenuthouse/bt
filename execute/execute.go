package execute

import (
	"bt/util"
	"bytes"
	"context"
	"fmt"
	"log"
	"os"
	"regexp"
	"strconv"
	"strings"
	"text/template"

	"bt/gencode"

	"bt/config"
	"bt/flag"
	"bt/parser"
	"bt/statement"
	"cloud.google.com/go/bigtable"
)

type (
	filter struct {
		Family        string
		Qualifier     string
		Value         string
		LatestNFilter int
	}

	initialize func(context.Context, *statement.Statement, flag.Flags, *os.File) (interface{}, error)
	process    func(context.Context, *statement.Statement, *bigtable.Table, *bigtable.Row, interface{}) (bool, error)
	cleanup    func(interface{}) (string, error)
)

func Process(out *os.File, cmd []string) (response, help, file2browser string, err error) {
	stmt, flags, err := parser.Parse(cmd)
	if err != nil {
		return
	}

	if flags.Debug() {
		response += stmt.Dump() + "\n"
	}

	// Is help needed after parsing?
	if subject, needHelp := flags.Help(); needHelp {
		help = util.FirstString("general", subject, stmt.Method)
		return
	}

	// Get the configuration
	var cfg config.Config
	cfg, err = config.Read()
	if err != nil {
		path, _ := config.FileName()
		err = fmt.Errorf("unable to read the config file (%s): %v", path, err)
		return
	}

	switch stmt.Method {
	case parser.TABLE:
		response, help, err = executeTable(cfg, &stmt, flags)
	case parser.INSERT:
	//	return createRecord(cfg, stmt, flags)
	case parser.UPDATE:
		response, help, file2browser, err = readUpdateDeleteRecord(out, cfg, &stmt, flags, updateInitialize, updateProcess, updateCleanup)
	case parser.DELETE:
		response, help, file2browser, err = readUpdateDeleteRecord(out, cfg, &stmt, flags, deleteInitialize, deleteProcess, deleteCleanup)
	case parser.READ:
		response, help, file2browser, err = readUpdateDeleteRecord(out, cfg, &stmt, flags, selectInitialize, selectProcess, selectCleanup)
	}

	return
}

func readUpdateDeleteRecord(out *os.File, cfg config.Config, stmt *statement.Statement, flags flag.Flags, initialize initialize, process process, cleanup cleanup) (string, string, string, error) {

	table := cfg.FindTable(stmt.TableName)

	// Determine the row filters
	rowFilters, err := buildRowFilters(stmt, table)
	if err != nil {
		return "", "", "", err
	}
	keyList, prefixList := buildQuery(rowFilters)

	// Determine the family:qualifier filters
	famQualFilters, filterCode, err := buildFamilyQualifierFilters(stmt)
	if err != nil {
		return "", "", "", err
	}

	readOptions := buildReadOptions(famQualFilters, stmt.Limit)

	// If formatted to display code
	if flags.Code() != "" {
		env, err := environment(flags, cfg)
		if err == nil {
			code, err := gencode.Generate(stmt.Method, stmt.TableName, env.Project, env.Instance, keyList, prefixList, filterCode, stmt.Sets, stmt.Limit)
			if err == nil {
				return code, "", "", nil
			}
		}
		return "", "", "", err
	}

	// Open bigtable client
	ctx := context.Background()
	client, err := openClient(ctx, flags, cfg)
	if err != nil {
		return "", "", "", fmt.Errorf("connecting: %v", err)
	}
	defer client.Close()

	data, err := initialize(ctx, stmt, flags, out)
	if err != nil {
		return "", "", "", fmt.Errorf("initialization: %v", err)
	}

	// Read from the table and process the rows
	btTable := client.Open(stmt.TableName)
	for _, rowSet := range prefixList {
		err := btTable.ReadRows(
			ctx,
			rowSet,
			func(row bigtable.Row) bool {
				cont, err := process(ctx, stmt, btTable, &row, data)
				if err != nil {
					log.Println(fmt.Errorf("processing: %v", err))
					return false
				}
				return cont
			},
			readOptions...,
		)
		if err != nil {
			return "", "", "", fmt.Errorf("reading: %v", err)
		}
	}

	if len(keyList) > 0 {
		err := btTable.ReadRows(
			ctx,
			keyList,
			func(row bigtable.Row) bool {
				cont, err := process(ctx, stmt, btTable, &row, data)
				if err != nil {
					log.Println(fmt.Errorf("processing: %v", err))
					return false
				}
				return cont
			},
			readOptions...,
		)
		if err != nil {
			return "", "", "", fmt.Errorf("reading: %v", err)
		}
	}

	file2browser, err := cleanup(data)

	// All done
	return "", "", file2browser, err
}

func buildReadOptions(famQualFilters bigtable.Filter, limit int64) []bigtable.ReadOption {
	options := []bigtable.ReadOption{ bigtable.RowFilter(famQualFilters)}
	if limit != 0 {
		options = append(options, bigtable.LimitRows(limit))
	}
	return options
}

func buildQuery(rowFilters []interface{}) (keyList bigtable.RowList, prefixList []bigtable.RowSet) {
	// Build the rowList (
	for _, keyFilter := range rowFilters {
		switch keyFilter.(type) {
		case string:
			keyList = append(keyList, keyFilter.(string))
		case bigtable.RowSet:
			prefixList = append(prefixList, keyFilter.(bigtable.RowSet))
		}
	}

	return
}

// buildRowFilters will create a key for the table
func buildRowFilters(stmt *statement.Statement, table *config.Table) ([]interface{}, error) {
	keys := []interface{}{}
	prefixes := []interface{}{}

	for i := range stmt.Filters {
		if strings.ToUpper(stmt.Filters[i].Field) == "PREFIX" {
			for _, value := range stmt.Filters[i].Values {
				prefixes = append(prefixes, bigtable.PrefixRange(value))
			}
			stmt.Filters[i].IsKeyArg = true
		}
		if strings.ToUpper(stmt.Filters[i].Field) == "KEY" {
			for _, value := range stmt.Filters[i].Values {
				keys = append(keys, value)
			}
			stmt.Filters[i].IsKeyArg = true
		}
	}

	if len(keys) != 0 {
		return keys, nil
	}
	if len(prefixes) != 0 {
		return prefixes, nil
	}

	if table == nil {
		return []interface{}{bigtable.PrefixRange("")}, nil
	}

	// Process table key patterns
	for _, keyInfo := range table.Keys {
		patternArgs := extractArgs(keyInfo.Pattern)
		matches, allMatch := stmt.MapWhereToArgs(patternArgs)
		if !allMatch {
			continue
		}

		keyFilters, err := buildKeyFiltersFromPattern(keyInfo.Pattern, matches)
		if err != nil {
			return nil, err
		}

		keys := []interface{}{}
		for _, filter := range keyFilters {
			if strings.ToUpper(keyInfo.Type) == "PREFIX" {
				keys = append(keys, bigtable.PrefixRange(filter))
			} else {
				keys = append(keys, filter)
			}
		}

		stmt.FlagKeyArgs(keyInfo.Pattern)
		return keys, nil
	}

	return []interface{}{bigtable.PrefixRange("")}, nil
}

func buildKeyFiltersFromPattern(pattern string, filters map[string]*statement.QueryFilter) ([]string, error) {
	type argIter struct {
		arg   string
		index int
		count int
	}

	// Initialize
	iters := []argIter{}
	for key, value := range filters {
		iters = append(iters, argIter{
			arg:   key,
			index: 0,
			count: len(value.Values),
		})
	}

	if len(iters) == 0 {
		return []string{}, nil
	}

	// Start
	ret := []string{}
	for {
		// initialize the interation
		strIndexes := []string{}
		values := map[string]interface{}{}
		for _, iter := range iters {
			strIndexes = append(strIndexes, strconv.Itoa(iter.index))
			values[iter.arg] = filters[iter.arg].Values[iter.index]
		}

		// build key
		key, err := resolveParameters("["+strings.Join(strIndexes, ",")+"]", pattern, values)
		if err != nil {
			return ret, err
		}

		ret = append(ret, key)

		// Increment
		done := false
		i := len(iters) - 1
		for {
			iters[i].index++
			if iters[i].index < iters[i].count {
				break
			}

			iters[i].index = 0
			if i == 0 {
				done = true
				break
			}

			i--
		}

		if done {
			break
		}
	}

	return ret, nil
}

func environment(flags flag.Flags, cfg config.Config) (*config.Environment, error) {
	param := flags.Environment()
	env := cfg.FindEnvironment(param)
	if env == nil {
		filename, _ := config.FileName()
		return nil, fmt.Errorf(`Unable to find "%s" environment. Check: %s`, param, filename)
	}

	return env, nil
}

func openClient(ctx context.Context, flags flag.Flags, cfg config.Config) (*bigtable.Client, error) {
	env, err := environment(flags, cfg)
	if err != nil {
		return nil, err
	}

	return bigtable.NewClient(ctx, env.Project, env.Instance)
}

func openAdmin(ctx context.Context, flags flag.Flags, cfg config.Config) (*bigtable.AdminClient, error) {
	param := flags.Environment()
	env := cfg.FindEnvironment(param)
	if env == nil {
		filename, _ := config.FileName()
		return nil, fmt.Errorf(`Unable to find "%s" environment. Check: %s`, param, filename)
	}

	fmt.Println("Admin connection to:", env.Name)
	return bigtable.NewAdminClient(ctx, env.Project, env.Instance)
}

func resolveParameters(name, pattern string, where map[string]interface{}) (string, error) {

	t, err := template.New(name).Parse(pattern)
	t.Option("missingkey=error")
	buf := new(bytes.Buffer)
	err = t.Execute(buf, where)
	return buf.String(), err
}

// FlagKeyArgs returns
func extractArgs(pattern string) []string {
	args := []string{}
	re := regexp.MustCompile(`(?m){{\.(\w*)}}`)
	for _, match := range re.FindAllStringSubmatch(pattern, -1) {
		args = append(args, match[1])
	}

	return args
}

func buildColumnFilter(stmt *statement.Statement) []filter {

	return []filter{filter{LatestNFilter: 1}}

	//for _, field := range stmt.Fields {
	//	newField := filter{LatestNFilter: 1}
	//	if !field.All {
	//		if field.Family != "" {
	//			newField.Family = field.Family
	//		}
	//		if field.Qualifier != "" {
	//			newField.Qualifier = field.Qualifier
	//		}
	//	}
	//
	//	ret = append(ret, newField)
	//}
	//
	//return ret, nil
}

func cleanForRegex(value string) string {
	return value
}

func buildFamilyQualifierFilters(stmt *statement.Statement) (bigtable.Filter, string, error) {

	filters := buildColumnFilter(stmt)

	for _, queryFilter := range stmt.Filters {
		if queryFilter.IsKeyArg {
			continue
		}

		split := strings.Split(queryFilter.Field, ":")
		if len(split) == 2 {
			switch queryFilter.Opr {
			case "=":
				filters = append(filters, filter{LatestNFilter: 1, Family: split[0], Qualifier: split[1], Value: queryFilter.Values[0]})
			case "CONTAINS":
				filters = append(filters, filter{LatestNFilter: 1, Family: split[0], Qualifier: split[1], Value: ".*" + cleanForRegex(queryFilter.Values[0]) + ".*"})
			}
		} else {
			return nil, "", fmt.Errorf("unable to determine the family:qualifier from %s. Use %s: for family or :%s for column", queryFilter.Field, queryFilter.Field, queryFilter.Field)
		}
	}

	var conditionFilter bigtable.Filter

	retCode := ""
	for _, filter := range filters {
		a := []bigtable.Filter{}
		aCode := []string{}
		if filter.LatestNFilter != 0 {
			a = append(a, bigtable.LatestNFilter(filter.LatestNFilter))
			aCode = append(aCode, fmt.Sprintf(`bigtable.LatestNFilter(%d)`, filter.LatestNFilter))
		}
		if filter.Family != "" {
			a = append(a, bigtable.FamilyFilter(filter.Family))
			aCode = append(aCode, fmt.Sprintf(`bigtable.FamilyFilter("%s")`, filter.Family))
		}
		if filter.Qualifier != "" {
			a = append(a, bigtable.ColumnFilter(filter.Qualifier))
			aCode = append(aCode, fmt.Sprintf(`bigtable.ColumnFilter("%s")`, filter.Qualifier))
		}
		if filter.Value != "" {
			a = append(a, bigtable.ValueFilter(filter.Value))
			aCode = append(aCode, fmt.Sprintf(`bigtable.ValueFilter("%s")`, filter.Value))
		}

		var f bigtable.Filter
		var fCode string
		if len(a) == 1 {
			f = a[0]
			fCode = aCode[0] + ","
		} else {
			f = bigtable.ChainFilters(a...)
			fCode = "\tbigtable.ChainFilters(\n\t\t\t" + strings.Join(aCode, ",\n\t\t\t") + ",\n\t\t),"
		}

		if conditionFilter == nil {
			conditionFilter = f
			retCode = fCode
		} else {
			conditionFilter = bigtable.ConditionFilter(f, conditionFilter, nil)
			retCode = fmt.Sprintf("bigtable.ConditionFilter(\n\t%s\n\t%s\n\tnil),", fCode, retCode)
		}
	}
	return conditionFilter, retCode, nil
}
