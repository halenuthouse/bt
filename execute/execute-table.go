package execute

import (
	"context"
	"fmt"

	"sort"
	"strings"

	"bt/config"
	"bt/flag"
	"bt/statement"
	"cloud.google.com/go/bigtable"
)

func executeTable(cfg config.Config, stmt *statement.Statement, flags flag.Flags) (string, string, error) {

	response := ""
	help := ""

	switch strings.ToUpper(stmt.SubFunction) {
	case "LIST":
		ctx := context.Background()
		admin, err := openAdmin(ctx, flags, cfg)
		defer admin.Close()

		if err != nil {
			return "", "", fmt.Errorf("connecting: %v", err)
		}

		tables, err := admin.Tables(ctx)
		if err != nil {
			return "", "", fmt.Errorf("unable to query the list of tables: %v", err)
		}

		sort.Strings(tables)
		response += "Table Name\n"
		response += "-----------\n"
		for _, tableName := range tables {
			response += tableName + "\n"
		}
	case "COUNT":
		if stmt.TableName == "" {
			return "", "", fmt.Errorf("Missing table name.  Usage: bt table count <tableName>")
		}

		ctx := context.Background()
		client, err := openClient(ctx, flags, cfg)
		defer client.Close()

		if err != nil {
			return "", "", fmt.Errorf("Error connecting: %v", err)
		}

		btTable := client.Open(stmt.TableName)
		count := 0

		err = btTable.ReadRows(
			ctx,
			bigtable.InfiniteRange(""),
			func(_ bigtable.Row) bool {
				count++
				if count % 1000 == 0 {
					fmt.Printf("Count: %d\r", count)
				}
				return true
			},
			bigtable.RowFilter(bigtable.StripValueFilter()),
		)
		if err != nil {
			return "", "", fmt.Errorf("Error reading rows: %v", err)
		}
		response = fmt.Sprintf("Record Count for", stmt.TableName, "is", count)

	case "SCHEMA":
		if stmt.TableName == "" {
			return "", "", fmt.Errorf("Missing table name.\nUsage: bt table schema <tableName>")
		}

		ctx := context.Background()
		admin, err := openAdmin(ctx, flags, cfg)
		defer admin.Close()

		if err != nil {
			return "", "", fmt.Errorf("Connecting: %v", err)
		}

		ti, err := admin.TableInfo(ctx, stmt.TableName)
		if err != nil {
			return "", "", fmt.Errorf("Getting table info: %v", err)
		}

		response = "Family Name" + strings.Repeat(" ", 19) + "GC Policy\n"
		response += strings.Repeat("-", 29) + " " + strings.Repeat("-", 20) + "\n"
		for _, fam := range ti.FamilyInfos {
			response += fmt.Sprintf("%-30s%s\n", fam.Name, fam.GCPolicy)
		}
	default:
		help = "table"
	}

	return response, help, nil
}
