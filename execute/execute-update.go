package execute

import (
	"context"
	"fmt"
	"os"

	"cloud.google.com/go/bigtable"
	"bt/flag"
	"bt/statement"
)

func updateInitialize(_ context.Context, _ *statement.Statement, _ flag.Flags, _ *os.File) (interface{}, error) {
	count := 0
	return &count, nil
}

func updateProcess(ctx context.Context, stmt *statement.Statement, table *bigtable.Table, row *bigtable.Row, data interface{}) (bool, error) {
	count := data.(*int)

	mut := bigtable.NewMutation()
	version := bigtable.Now()

	for _, set := range stmt.Sets {
		mut.Set(set.Family, set.Qualifier, version, set.Value)
	}

	err := table.Apply(ctx, row.Key(), mut)
	if err == nil {
		*count++
	}
	return true, err
}

func updateCleanup(data interface{}) (string, error) {
	count := data.(*int)

	fmt.Println("Updated Rows:", *count)
	return "", nil
}
