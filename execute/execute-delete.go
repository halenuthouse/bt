package execute

import (
	"context"
	"fmt"
	"os"

	"cloud.google.com/go/bigtable"
	"bt/flag"
	"bt/statement"
)

func deleteInitialize(_ context.Context, _ *statement.Statement, _ flag.Flags, _ *os.File) (interface{}, error) {
	count := 0
	return &count, nil
}

func deleteProcess(ctx context.Context, _ *statement.Statement, table *bigtable.Table, row *bigtable.Row, data interface{}) (bool, error) {
	count := data.(*int)

	mut := bigtable.NewMutation()
	mut.DeleteRow()
	err := table.Apply(ctx, row.Key(), mut)
	if err == nil {
		*count++
	}
	return true, err
}

func deleteCleanup(data interface{}) (string, error) {
	count := data.(*int)
	fmt.Println("Deleted Rows:", *count)
	return "", nil
}
