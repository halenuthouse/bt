package execute

import (
	"fmt"
	"testing"

	"bt/statement"
)

// Test_buildkeyFiltersFromPattern tests
func Test_buildKeyFiltersFromPattern(t *testing.T) {
	rawArgs := [][]string{
		{"argument1"},
		{"argument2-1", "argument2-2"},
		{"argument3-1", "argument3-2", "argument3-3"},
		{"argument4-1", "argument4-2"},
	}

	args := map[string]*statement.QueryFilter{}
	for i, argValues := range rawArgs {
		field := fmt.Sprintf("arg%d", i+1)
		args[field] = &statement.QueryFilter{
			Field:    field,
			Opr:      "=",
			Values:   argValues,
			IsKeyArg: false,
		}
	}

	tests := []struct {
		name     string
		pattern  string
		expected []string
	}{
		{name: "test1", pattern: "com#{{.arg1}}", expected: []string{
			"com#argument1",
		}},

		{name: "test2", pattern: "com#{{.arg2}}", expected: []string{
			"com#argument2-1",
			"com#argument2-2",
		}},

		{name: "test3", pattern: "com#{{.arg1}}#{{.arg2}}", expected: []string{
			"com#argument1#argument2-1",
			"com#argument1#argument2-2",
		}},

		{name: "test4", pattern: "com#{{.arg2}}#{{.arg1}}", expected: []string{
			"com#argument2-1#argument1",
			"com#argument2-2#argument1",
		}},

		{name: "test5", pattern: "com#{{.arg2}}#{{.arg1}}#{{.arg3}}", expected: []string{
			"com#argument2-1#argument1#argument3-1",
			"com#argument2-1#argument1#argument3-1",
			"com#argument2-1#argument1#argument3-2",
			"com#argument2-1#argument1#argument3-2",
			"com#argument2-1#argument1#argument3-3",
			"com#argument2-1#argument1#argument3-3",
			"com#argument2-2#argument1#argument3-1",
			"com#argument2-2#argument1#argument3-1",
			"com#argument2-2#argument1#argument3-2",
			"com#argument2-2#argument1#argument3-2",
			"com#argument2-2#argument1#argument3-3",
			"com#argument2-2#argument1#argument3-3",
		}},

		{name: "test6", pattern: "com#{{.arg1}}#{{.arg2}}#{{.arg3}}#{{.arg4}}", expected: []string{
			"com#argument1#argument2-1#argument3-1#argument4-1",
			"com#argument1#argument2-2#argument3-1#argument4-1",
			"com#argument1#argument2-1#argument3-2#argument4-1",
			"com#argument1#argument2-2#argument3-2#argument4-1",
			"com#argument1#argument2-1#argument3-3#argument4-1",
			"com#argument1#argument2-2#argument3-3#argument4-1",
			"com#argument1#argument2-1#argument3-1#argument4-2",
			"com#argument1#argument2-2#argument3-1#argument4-2",
			"com#argument1#argument2-1#argument3-2#argument4-2",
			"com#argument1#argument2-2#argument3-2#argument4-2",
			"com#argument1#argument2-1#argument3-3#argument4-2",
			"com#argument1#argument2-2#argument3-3#argument4-2",
		}},

		{name: "test7", pattern: "com#{{.arg4}}#{{.arg2}}#{{.arg3}}#{{.arg1}}", expected: []string{
			"com#argument4-1#argument2-1#argument3-1#argument1",
			"com#argument4-1#argument2-2#argument3-1#argument1",
			"com#argument4-1#argument2-1#argument3-2#argument1",
			"com#argument4-1#argument2-2#argument3-2#argument1",
			"com#argument4-1#argument2-1#argument3-3#argument1",
			"com#argument4-1#argument2-2#argument3-3#argument1",
			"com#argument4-2#argument2-1#argument3-1#argument1",
			"com#argument4-2#argument2-2#argument3-1#argument1",
			"com#argument4-2#argument2-1#argument3-2#argument1",
			"com#argument4-2#argument2-2#argument3-2#argument1",
			"com#argument4-2#argument2-1#argument3-3#argument1",
			"com#argument4-2#argument2-2#argument3-3#argument1",
		}},
	}

	for _, test := range tests {
		testArgs := map[string]*statement.QueryFilter{}
		for _, patternArg := range extractArgs(test.pattern) {
			testArgs[patternArg] = args[patternArg]
		}

		keyFilters, err := buildKeyFiltersFromPattern(test.pattern, testArgs)
		if err != nil {
			t.Error(err)
		}

		//if len(test.expected) != len(keyFilters) {
		//	t.Error(fmt.Sprintf("%s: Expected %d results, but got %d", test.name, len(test.expected), len(keyFilters)))
		//}

		for _, expected := range test.expected {
			found := false
			for _, keyFilter := range keyFilters {
				if expected == keyFilter {
					found = true
					break
				}
			}

			if !found {
				t.Errorf("%s: didn't find %s", test.name, expected)
			}
		}

		for _, keyFilter := range keyFilters {
			found := false
			for _, expected := range test.expected {
				if expected == keyFilter {
					found = true
					break
				}
			}

			if !found {
				t.Errorf("%s: found %s which was unexpected", test.name, keyFilter)
			}
		}
	}
}
